![ALT](/source/images/ergstick_logo.png)

# ErgStickSDK
ErgStickSDK is a framework that allows connection and data retrival from Concept2 PM monitors directly or via ErgStick. Also, it allows connection to BLE enabled Heart Rate monitor directly, or ANT+ Heart Rate monitor via ErgStick.

## Installation

### Cocoapods
[CocoaPods][identifier1] is a dependency manager for Cocoa projects. You can install it with the following command:
```ruby
$ sudo gem install cocoapods
```
To integrate ErgStickSDK into your Xcode project using CocoaPods, specify it in your `Podfile`:
```ruby
platform :ios, '12.0'
use_frameworks!

target 'Your Target Name' do
    pod 'ErgStickSDK'
end
```
Then, run the following command:
```ruby
$ pod install
```

### Carthage
[Carthage][identifier2] is a decentralized dependency manager that builds your dependencies and provides you with binary frameworks.
You can install it by downloading and running `Carthage.pkg` available [here][identifier3].

To integrate ErgStickSDK into your Xcode project using Carthage, specify it in your `Cartfile`:
```ruby
git "https://gitlab.com/endurancesports/ErgStickSDKiOS.git"
```
Then, run the following command:
```ruby
$ carthage update --platform iOS
```
Drag `ErgStickSDK.framework` and its dependencies (`iOSDFULibrary.framework` and `Zip.framework`) into your Xcode project.

<!-- Identifiers, in alphabetical order -->
[identifier1]: https://cocoapods.org
[identifier2]: https://github.com/Carthage/Carthage
[identifier3]: https://github.com/Carthage/Carthage/releases

----

## Usage
The framework is based on Singleton and Observer patterns.

### Connection
`PeripheralManager` is a singleton used for managing connection between with BLE enabled peripheral (PM monitor, ErgStick or Heart Rate monitor). It can be accessed with `shared` instance.

Scanning for rowing monitor (PM monitor or ErgStick):

```swift
import ErgStickSDK

PeripheralManager.shared.scanForRowingMonitor()
```

Other methods for managing connection:
```swift
func scanForBLEHRMonitor()
func scanForANTPlusHRMonitor()
func stopScan()
func connectToPeripheral(at index: Int)
func disconnectFromRowingMonitor()
func disconnectFromBLEHRMonitor()
func disconnectFromANTPlusHRMonitor()
func cleanUpDiscoveredPeripherals()
```

### Checking state
`PeripheralManager` has gettable only properties for checking current state of connection or workout, and for getting current BPM of directly connected Heart Rate monitor:

```swift
var isConnectedToRowingMonitor: Bool { get }
var isConnectedToPM5: Bool { get }
var isConnectedToErgStick: Bool { get }
var isConnectedToBLEHRMonitor: Bool { get }
var isConnectedToANTPlusHRMonitor: Bool { get }
var isWorkoutRunning: Bool { get }
var heartRateMonitorBPM: Float { get }
```

### Data retrival
In order to get a list of discovered peripherals add observer for  `didDiscoverPeripheral` notification.

```swift
NotificationCenter.default.addObserver(self, selector: #selector(reloadPeripherals), name: .didDiscoverPeripheral, object: nil)

@objc func reloadPeripherals(_ notification: Notification) {
  guard let peripherals = notification.object as? [String] else { return }
  self.peripherals = peripherals
}
```

You can add observer for these events:
```swift
var peripheralConnectivityDidChange: Notification.Name { get }
var newErgStickFirmwareAvailable: Notification.Name { get }
var newPMFirmwareAvailable: Notification.Name { get }
var newPM5FirmwareAvailable: Notification.Name { get }
```

In order to get real time rowing data subscribe to:
```swift
var newRowingDataAvailable: Notification.Name { get }
var newForceCurveDataAvailable: Notification.Name { get }
var resetRowingData: Notification.Name { get }
```

Real time data is encapsulated in a struct `RowingData`. It has `String` properties that contain already formatted data (elapsed time, distance, pace...) ready for displaying.
```swift
NotificationCenter.default.addObserver(self, selector: #selector(updateUI(_:)), name: .newRowingDataAvailable, object: nil)
NotificationCenter.default.addObserver(self, selector: #selector(updateUI(_:)), name: .resetRowingData, object: nil)

@objc func updateUI(_ notification: Notification) {
  guard let data = notification.object as? RowingData else { return }

  // display real time data

  labelPrimaryValue.text = data.primaryValue
  labelPrimaryUnits.text = data.primaryUnits
  labelSecondaryValue.text = data.secondaryValue
  labelSecondaryUnits.text = data.secondaryUnits

  labelCurrentPace.text = data.currentPace
  labelAveragePace.text = data.averagePace

  // ...
}
```

After session has been recorded, session file in JSON format is sent with notification `didRecordSession`.
```swift
NotificationCenter.default.addObserver(self, selector: #selector(handleSession(_:)), name: .didRecordSession, object: nil)

@objc func handleSession(_ notification: Notification) {
  guard let data = notification.object as? Data else { return }

  // handle data
}
```

Session file structure is as follows (sample data is used):

```
{
  "guid": "5af9b6d4cf1e8c5b8d5ea7a1-4-5902-1526256000-265542",
  "device_identifier": "PM5 430213885",
  "device_firmware": "159",
  "program_id": 0,
  "workout_type": 3,
  "elapsed_time": 538,
  "distance": 2000,
  "avg_pace": 144,
  "avg_power": 134,
  "avg_stroke_rate": 27,
  "ending_heart_rate": 159,
  "avg_heart_rate": 147,
  "min_heart_rate": 95,
  "max_heart_rate": 160,
  "drag_factor_avg": 90,
  "created_at": "2018-05-14 00:00:00",
  "intervals": [
    {
      "internal_id": 1,
      "interval_type": 1,
      "planned_distance_or_time": 2000,
      "elapsed_distance": 500,
      "elapsed_time": 133.7,
      "rest_time": 0,
      "rest_distance": 0,
      "avg_stroke_rate": 27,
      "avg_pace": 133.7,
      "avg_power": 146,
      "avg_heart_rate": 127,
      "total_calories": 0,
      "avg_calories": 0,
      "avg_speed": 0,
      "avg_drag_factor": 0,
      "samples": [
        {
          "stroke_internal_id": 1,
          "seconds_passed": 1.2300000191,
          "elapsed_time": 1.230000019074,
          "distance": 3.799999952316,
          "stroke_rate": 0,
          "speed": 0,
          "workout_state": 1,
          "rowing_state": 1,
          "stroke_state": 4,
          "total_work_distance": 0,
          "workout_duration": 20,
          "workout_distance": 2000,
          "drag_factor": 0,
          "current_pace": 0,
          "avg_pace": 0,
          "rest_distance": 0,
          "rest_time": 0,
          "total_avg_power": "",
          "total_avg_calories": "",
          "split_avg_pace": 0,
          "split_avg_power": 0,
          "split_avg_calories": 0,
          "last_split_time": 0,
          "last_split_distance": 0,
          "current_heart_rate": 0
        },
        {
          ...
        }
      ]
    }
  ],
  "strokes": [
    {
      "internal_id": 1,
      "drive_length": 1.02999997139,
      "drive_time": 0.819999992847,
      "recovery_time": 0,
      "distance": 2.419999837875,
      "power": 70,
      "time_to_peak": 0,
      "peak_drive_force": 116.40000152588,
      "avg_drive_force": 84.800003051758,
      "work_per_stroke": 6553.5,
      "calories": 541,
      "projected_work_time": 683,
      "projected_work_distance": 0,
      "plot": [0.0, 15.0, 15.400001, 16.0, 16.1, 16.5, 17.2, 17.800001, 18.0, 17.800001, 17.300001, 16.9, 15.0, 14.1, 12.0, 9.8, 6.2000003, 4.9, 1.2, 0.0, 15.1, 15.1, 15.5, 15.6, 16.1, 16.1, 16.4, 16.0, 15.6, 15.2, 14.400001, 13.400001, 13.0, 13.3, 12.5, 11.5, 9.6, 9.1, 7.7000003, 6.2000003, 5.2000003, 3.5, 2.5, 13.1, 13.2, 13.3, 13.900001, 15.0, 15.5, 15.2, 14.900001, 13.6, 12.900001, 12.2, 11.400001, 10.1, 10.1, 9.3, 8.2]
    },
    {
      ...
    }
  ]
}
```
