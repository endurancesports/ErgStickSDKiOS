Pod::Spec.new do |s|
  s.name = 'ErgStickSDK'
  s.version = '1.3.2'
  s.license = 'BSD 3-Clause'
  s.homepage = 'https://gitlab.com/endurancesports/ErgStickSDKiOS'
  s.authors = { 'Nikola Milicevic' => 'nikola.dzoni@gmail.com' }
  s.summary = 'Framework that allows connection and data retrival from Concept2 PM monitors directly or via ErgStick.'
  s.source = { :git => 'https://gitlab.com/endurancesports/ErgStickSDKiOS.git', :tag => s.version.to_s }
  s.social_media_url = 'https://ergstick.com'
  s.swift_version = '5.0'
  s.ios.deployment_target = '12.0'
  s.source_files = 'ErgStickSDK/*'
  s.dependency 'iOSDFULibrary'
  s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
end
