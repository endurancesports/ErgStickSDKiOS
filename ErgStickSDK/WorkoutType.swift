//
//  WorkoutType.swift
//  Float
//
//  Created by Nikola Milicevic on 6/20/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

@objc public enum WorkoutType: Int16, Codable {
	case justRowNoSplits
	case justRowSplits
	case fixedDistanceNoSplits
	case fixedDistanceSplits
	case fixedTimeNoSplits
	case fixedTimeSplits
	case fixedTimeIntervals
	case fixedDistanceIntervals
	case variableIntervals
	case variableIntervalsRestUndefined
	case fixedCalories
	case fixedWattMinutes
	case fixedCaloriesIntervals
	case num
	
	public var isJustRow: Bool {
		return self == .justRowSplits || self == .justRowNoSplits
	}
	public var isDistanceBased: Bool {
		return self == .justRowSplits || self == .justRowNoSplits || self == .fixedDistanceSplits || self == .fixedDistanceNoSplits || self == .fixedDistanceIntervals
	}
	public var isIntervalBased: Bool {
		return self == .fixedTimeIntervals || self == .fixedDistanceIntervals || self == .variableIntervals || self == .variableIntervalsRestUndefined
	}
}
