//
//  IntervalBuffer.swift
//  Float
//
//  Created by Nikola Milicevic on 6/20/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

class IntervalBuffer: Codable {
	var internalId: Int?
	
	// split interval data
	var intervalType: IntervalType?
	var elapsedTime: Float = 0
	var elapsedDistance: Float = 0
	var restTime: Float = 0
	var restDistance: Float = 0
	
	// additional split interval data
	var avgStrokeRate: Float = 0
	var avgHeartRate: Float = 0
	var avgPace: Float = 0
	var totalCalories: Float = 0
	var avgCalories: Float = 0
	var avgSpeed: Float = 0
	var avgPower: Float = 0
	var avgDragFactor: Float = 0
	
	var plannedDistanceOrTime: Float?
	
	var samples: [SampleBuffer]?
}
