//
//  WorkoutState.swift
//  Float
//
//  Created by Nikola Milicevic on 6/20/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

@objc public enum WorkoutState: Int16, Codable {
	enum WorkoutStateType {
		case waitToBegin
		case running
		case ended
		case terminated
		case logged
	}
	
	case waitToBegin = 0
	case row = 1
	case countDownPause
	case intervalRest
	case intervalWorkTime
	case intervalWorkDistance
	case intervalRestEndToWorkTime
	case intervalRestEndToWorkDistance
	case intervalWorkTimeToRest
	case intervalWorkDistanceToRest
	case end = 10
	case terminate = 11
	case logged = 12
	case rearm
	
	var state: WorkoutStateType {
		switch self.rawValue {
		case 0: return .waitToBegin
		case 1...9: return .running
		case 10: return .ended
		case 11: return .terminated
		case 12: return .logged
		default: return .waitToBegin
		}
	}
	
	public var displayString: String {
		switch state {
		case .waitToBegin: return "Wait To Begin"
		case .running: return "Running"
		case .ended: return "Ended"
		case .terminated: return "Terminated"
		case .logged: return "Logged"
		}
	}
}
