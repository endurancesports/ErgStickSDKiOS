//
//  SessionNotifier.swift
//  Float
//
//  Created by Nikola Milicevic on 5/24/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

public struct RowingData {
	public let primaryValue: String
	public let primaryUnits: String
	public let secondaryValue: String
	public let secondaryUnits: String
	
	public let currentPace: String
	public let averagePace: String
	public let heartRate: String
	public let strokeRate: String
	public let strokeCount: String
	
	public let driveLength: String
	public let driveRatio: String
	public let strokePower: String
	public let averageForce: String
	public let peakForce: String
	public let driveTime: String
	public let driveSpeed: String
	public let dragFactor: String
}

class SessionNotifier {
	func notify(_ data: RowingBuffer) {
		let pv: String
		let pu: String
		let sv: String
		let su: String
		
		if data.intervalType.isDistanceBased {
			if data.workoutType.isJustRow {
				pv = floor(data.distance).string(decimals: 0)
			} else {
				pv = (data.workoutDistance - floor(data.distance)).string(decimals: 0)
			}
			pu = "m"
			sv = data.elapsedTime.hoursMinutesSeconds
			su = "time elapsed"
			
		} else if data.intervalType.isTimeBased {
			pv = (data.workoutDuration - floor(data.elapsedTime)).hoursMinutesSeconds
			pu = ""
			sv = floor(data.distance).string(decimals: 0)
			su = "m"
			
		// rest
		} else {
			pv = data.restTime.hoursMinutesSeconds
			pu = "r"
			sv = ""
			su = ""
		}
		
		let curPace: String
		if data.intervalType.isRestInterval {
			curPace = "Interval \(data.intervalCount)"
		} else {
			curPace = data.currentPace.minutesSeconds
		}
		
		let avgPace = data.avgPace.minutesSecondsDetail
		let hr = data.currentHeartRate.string(decimals: 0)
		let sRate = "\(data.strokeRate)"
		let sCount = "\(data.strokeCount)"
		let driveL = data.driveLength.string(decimals: 2)
		
		let driveR: String
		if data.driveTime > 0.0001 {
			driveR = (data.strokeRecoveryTime / data.driveTime).string(decimals: 2)
		} else {
			driveR = "-"
		}
		let sPower = data.strokePower.string(decimals: 0)
		let avgForce = data.avgDriveForce.properUnits.string(decimals: 1)
		let pForce = data.peakDriveForce.properUnits.string(decimals: 1)
		let driveT = data.driveTime.string(decimals: 2)
		let driveS = data.speed.string(decimals: 1)
		let dragF = data.dragFactor.string(decimals: 0)
		
		let rowingData = RowingData(primaryValue: pv, primaryUnits: pu, secondaryValue: sv, secondaryUnits: su, currentPace: curPace, averagePace: avgPace, heartRate: hr, strokeRate: sRate, strokeCount: sCount, driveLength: driveL, driveRatio: driveR, strokePower: sPower, averageForce: avgForce, peakForce: pForce, driveTime: driveT, driveSpeed: driveS, dragFactor: dragF)
		
		NotificationCenter.default.post(name: .newRowingDataAvailable, object: rowingData)
	}
	
	func notify(_ forcePlot: [Float]) {
		NotificationCenter.default.post(name: .newForceCurveDataAvailable, object: forcePlot)
	}
	
	func notifyReset(_ data: RowingBuffer, toZero: Bool = false) {
		let pv: String
		let pu: String
		let sv: String
		let su: String
		
		if data.intervalType.isDistanceBased {
			if toZero {
				pv = "0"
			} else {
				pv = "\(Int(data.workoutDistance))"
			}
			pu = "m"
			sv = "0:00"
			su = "time elapsed"

		} else {
			if toZero {
				pv = "0:00"
			} else {
				pv = data.workoutDuration.hoursMinutesSeconds
			}
			pu = ""
			sv = "0"
			su = "m"
		}
		
		let curPace = "0:00"
		let avgPace = "0:00.0"
		let hr = "0"
		let sRate = "0"
		let sCount = "0"
		
		let driveL = "0"
		let driveR = "0"
		let sPower = "0"
		let avgForce = "0"
		let pForce = "0"
		let driveT = "0"
		let driveS = "0"
		let dragF = "0"

		let rowingData = RowingData(primaryValue: pv, primaryUnits: pu, secondaryValue: sv, secondaryUnits: su, currentPace: curPace, averagePace: avgPace, heartRate: hr, strokeRate: sRate, strokeCount: sCount, driveLength: driveL, driveRatio: driveR, strokePower: sPower, averageForce: avgForce, peakForce: pForce, driveTime: driveT, driveSpeed: driveS, dragFactor: dragF)

		NotificationCenter.default.post(name: .resetRowingData, object: rowingData)
	}
}
