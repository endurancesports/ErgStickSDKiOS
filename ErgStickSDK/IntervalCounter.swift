//
//  IntervalCounter.swift
//  Float
//
//  Created by Nikola Milicevic on 6/13/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

class IntervalCounter {
	private var previousIntervalCount: Int = 0
    private var previousIntervalType: IntervalType? = nil
	
	private var splitLength: Float = 0.0
	
    var hasStartedNextInterval: Bool = false
	
	func getIntervalCount(from sampleBuffer: RowingBuffer) {
		// single distance
		if sampleBuffer.workoutType == .fixedDistanceSplits {
			if sampleBuffer.intervalCount == 0 {
				splitLength = sampleBuffer.distance.roundToNearest10
			} else {
				if splitLength != 0 {
					let currentIntervalCount = Int(sampleBuffer.distance / splitLength)
					hasStartedNextInterval = currentIntervalCount > previousIntervalCount
                    previousIntervalCount = currentIntervalCount
                    
					sampleBuffer.intervalCount = Int(sampleBuffer.distance / splitLength)
				}
			}
			
		// single time
		} else if sampleBuffer.workoutType == .fixedTimeSplits {
			if sampleBuffer.intervalCount == 0 {
				splitLength = sampleBuffer.elapsedTime.roundToNearest10
			} else {
				if splitLength != 0 {
					let currentIntervalCount = Int(sampleBuffer.elapsedTime / splitLength)
                    hasStartedNextInterval = currentIntervalCount > previousIntervalCount
					previousIntervalCount = currentIntervalCount
                    
					sampleBuffer.intervalCount = Int(sampleBuffer.elapsedTime / splitLength)
				}
			}
        } else if sampleBuffer.workoutType == .variableIntervalsRestUndefined {
            //print("IntervalCounter: \(sampleBuffer.intervalCount), oldType: \(previousIntervalType?.displayString), newType: \(sampleBuffer.intervalType.displayString)")
            previousIntervalCount = sampleBuffer.intervalCount
            guard let previousIntervalType = previousIntervalType else {
                self.previousIntervalType = sampleBuffer.intervalType
                return
            }
            
            hasStartedNextInterval = (previousIntervalType == .restUndefined && previousIntervalType != sampleBuffer.intervalType)
            self.previousIntervalType = sampleBuffer.intervalType
            
		} else {
			let currentIntervalCount = sampleBuffer.intervalCount
            hasStartedNextInterval = currentIntervalCount > previousIntervalCount
            previousIntervalCount = currentIntervalCount
		}
	}
}
