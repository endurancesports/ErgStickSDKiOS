//
//  PeripheralManager.swift
//  ErgStick
//
//  Created by Nikola Milicevic on 12/8/17.
//  Copyright © 2017 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

public class PeripheralManager {
	
	// MARK: - Init
	
	private init() {}
	
	// MARK: - Properties
	
	public static let shared = PeripheralManager()
	private let service = BluetoothService()
	
	var ergStickFirmware: ErgStickFirmware?
	
	// MARK: - State
	
	public var isConnectedToRowingMonitor: Bool {
		return service.rowingMonitor != nil
	}
	
	public var isConnectedToPM5: Bool {
		return service.rowingMonitor is PM5
	}
	
	public var isConnectedToErgStick: Bool {
		return service.rowingMonitor is ErgStick
	}
	
	public var isConnectedToBLEHRMonitor: Bool {
		return service.heartRateMonitor != nil
	}
	
	public var isConnectedToANTPlusHRMonitor: Bool {
		return (service.rowingMonitor as? ErgStick)?.antPlusHRMonitor != nil
	}
	
	public var isWorkoutRunning: Bool {
		if let pm5 = service.rowingMonitor as? PM5 {
			return pm5.workoutState.isRunning
		} else if let ergStick = service.rowingMonitor as? ErgStick {
			return ergStick.workoutState.isRunning
		} else {
			return false
		}
	}
	
	public var heartRateMonitorBPM: Float {
		return service.heartRateMonitor!.bpm
	}
	
	// MARK: - Scanning
	
	public func scanForRowingMonitor() {
		service.scan(for: .rowingMonitor)
	}
	
	public func scanForBLEHRMonitor() {
		service.scan(for: .heartRateMonitor)
	}
	
	public func scanForANTPlusHRMonitor() {
		(service.rowingMonitor as? ErgStick)?.connectToANTPlusHRMonitor()
	}
	
	public func stopScan() {
		service.stopScan()
	}
	
	// MARK: - Connection
	
	public func connectToPeripheral(at index: Int) {
		service.connect(at: index)
	}
	
	public func disconnectFromRowingMonitor() {
		service.disconnect(from: .rowingMonitor)
	}
	
	public func disconnectFromBLEHRMonitor() {
		service.disconnect(from: .heartRateMonitor)
	}
	
	public func disconnectFromANTPlusHRMonitor() {
		service.disconnect(from: .rowingMonitor)
	}
	
	// MARK: - Cleanup
	
	public func cleanUpDiscoveredPeripherals() {
		service.cleanUp()
	}
	
	// MARK: - Types
	
	public enum WeightUnits {
		case metric
		case imperial
	}
	
	// MARK: - Display Units
	
	public var weightUnits: WeightUnits = .metric
}
