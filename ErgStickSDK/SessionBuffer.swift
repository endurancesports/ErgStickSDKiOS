//
//  SessionBuffer.swift
//  Float
//
//  Created by Nikola Milicevic on 6/20/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

class SessionBuffer: Codable {
	var guid: String?
	var deviceIdentifier: String?
	var deviceFirmware: String?
	var programId: Int?
	var workoutType: WorkoutType?
	var createdAt: String?
	
	var elapsedTime: Float = 0
	var distance: Float = 0
	var avgPace: Float = 0
	var avgPower: Float = 0
	var avgStrokeRate: Float = 0
	var endingHeartRate: Float = 0
	var avgHeartRate: Float = 0
	var minHeartRate: Float = 0
	var maxHeartRate: Float = 0
	var dragFactorAvg: Float = 0
	
	var intervals: [IntervalBuffer]?
	var strokes: [StrokeBuffer]?
}
