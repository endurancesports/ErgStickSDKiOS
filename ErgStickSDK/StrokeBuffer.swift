//
//  StrokeBuffer.swift
//  Float
//
//  Created by Nikola Milicevic on 6/20/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

struct StrokeBuffer: Codable {
	let internalId: Int
	
	var driveLength: Float
	var driveTime: Float
	var recoveryTime: Float
	let distance: Float
	var peakDriveForce: Float
	var avgDriveForce: Float
	let workPerStroke: Float
	
	let power: Float
	var timeToPeak: Float
	let calories: Float
	let projectedWorkTime: Float
	let projectedWorkDistance: Float
	
	var plot: [Float]?
}
