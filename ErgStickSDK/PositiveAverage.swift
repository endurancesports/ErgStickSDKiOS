//
//  PositiveAverage.swift
//  ErgStick
//
//  Created by Nikola Milicevic on 2/12/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

class PositiveAverage {
	private var count: Int = 0
	private var sum: Float = 0.0
	
	func add(value: Float) {
		guard value > 0 else { return }
		sum += value
		count += 1
	}
	
	func getAverage() -> Float {
		guard count > 0 else { return 0.0 }
		return sum / Float(count)
	}
	
	func getTotal() -> Float {
		return sum
	}
}

class WorkoutRunningAverage {
	private var count: Int = 0
	private var sum: Float = 0.0
	
	func add(value: Float, workoutState: Int16) {
		if workoutState >= 1 && workoutState <= 9 {
			sum += value
			count += 1
		}
	}
	
	func getAverage() -> Float {
		if count > 0 {
			return sum / Float(count)
		} else {
			return 0.0
		}
	}
}

class HeartRateAverage {
	private var count: Int = 0
	private var sum: Float = 0.0
	
	private var min: Float = 1000.0
	private var max: Float = 0.0
	private var last: Float = 0.0
	
	private var countSplit: Int = 0
	private var sumSplit: Float = 0.0
	
	func add(value: Float, workoutState: WorkoutState.WorkoutStateType) {
		guard value > 0, workoutState == .running else { return }

		sum += value
		count += 1
		
		sumSplit += value
		countSplit += 1
		
		last = value
		
		if value < min {
			min = value
		}
		
		if value > max {
			max = value
		}
	}
	
	func getAverage() -> Float {
		guard count > 0 else { return 0.0 }
		return round(sum / Float(count))
	}
	
	func getSplitAverage() -> Float {
		guard countSplit > 0 else { return 0.0 }
		
		defer {
			countSplit = 0
			sumSplit = 0.0
		}
		
		return round(sumSplit / Float(countSplit))
	}
	
	func getMin() -> Float {
		if min == 1000 { return 0 }
		return min
	}
	
	func getMax() -> Float {
		return max
	}
	
	func getLast() -> Float {
		return last
	}
}
