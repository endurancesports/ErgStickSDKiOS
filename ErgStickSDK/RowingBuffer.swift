//
//  RowingBuffer.swift
//  Float
//
//  Created by Nikola Milicevic on 6/20/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

class RowingBuffer {
	// general status
	var elapsedTime: Float = 0
	var distance: Float = 0
	var workoutType: WorkoutType = .justRowNoSplits
	var intervalType: IntervalType = .time
	var workoutState: WorkoutState = .waitToBegin
	var rowingState: RowingState = .inactive
	var strokeState: StrokeState = .waitingToReachMinimumSpeed
	var totalWorkDistance: Float = 0
	var workoutDuration: Float = 0
	var workoutDistance: Float = 0
	var dragFactor: Float = 0
	
	// additional status 1
	var speed: Float = 0
	var strokeRate: Int = 0
	var currentHeartRate: Float = 0
	var currentPace: Float = 0
	var avgPace: Float = 0
	var restDistance: Float = 0
	var restTime: Float = 0
	
	// additional status 2
	var intervalCount: Int = 0
	var totalAvgPower: Float = 0
	var totalAvgCalories: Float = 0
	var splitAvgPace: Float = 0
	var splitAvgPower: Float = 0
	var splitAvgCalories: Float = 0
	var lastSplitTime: Float = 0
	var lastSplitDistance: Float = 0
	
	// stroke data
	var driveLength: Float = 0
	var driveTime: Float = 0
	var strokeRecoveryTime: Float = 0
	var strokeDistance: Float = 0
	var peakDriveForce: Float = 0
	var avgDriveForce: Float = 0
	var workPerStroke: Float = 0
	var strokeCount: Int = 0
	
	// additional stroke data
	var strokePower: Float = 0
	var strokeCalories: Float = 0
	var projectedWorkTime: Float = 0
	var projectedWorkDistance: Float = 0
	
	// ergstick resolvers helper properties
	var splitIntervalTime: Float = 0
	var splitIntervalDistance: Float = 0
	
	var splitIntervalAverageStrokeRate: Float = 0
	var splitIntervalAveragePace: Float = 0
	var splitIntervalAverageCalories: Float = 0
	var splitIntervalTotalCalories: Float = 0
	var splitIntervalSpeed: Float = 0
	var splitIntervalAverageDragFactor: Float = 0
	
	// force curve
	var plot = [Float]()
}
