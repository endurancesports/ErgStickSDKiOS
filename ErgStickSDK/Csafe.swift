//
//  Csafe.swift
//  ErgStick
//
//  Created by Nikola Milicevic on 1/24/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

struct Csafe {
	static func make(from data: [UInt8]) -> [UInt8]? {
		let len = data.count
		
		if len >= (MAX_BUFFER - 4) {
			print("Data too long! Max \(MAX_BUFFER-4)!")
			return nil
		}
		
		var newData = [UInt8]()
		
		let chk = checksum(from: data)
		for (i, _) in data.enumerated() {
			newData.insert(data[i], at: i)
		}
		newData.insert(chk, at: len)
		
		guard let stuffedData = stuff(newData) else { return nil }
		
		var finalData = [UInt8]()
		finalData.append(0xF1)
		finalData.append(contentsOf: stuffedData)
		finalData.append(0xF2)
		
		return finalData
	}
	
	static func get(from data: [UInt8]) -> Data? {
		guard let extracted = extract(from: data) else { return nil }
		return Data(bytes: destuff(extracted))
	}
}

private extension Csafe {
	static let MAX_BUFFER = 128
	
	static func checksum(from data: [UInt8]) -> UInt8 {
		var checksum: UInt8 = 0x00
		for x in data {
			checksum ^= x
		}
		return checksum
	}
	
	static func stuff(_ data: [UInt8]) -> [UInt8]? {
		var newData = [UInt8]()
		var idx = 0
		
		for b in data {
			if (b & 0xFC) == 0xF0 {
				newData.insert(0xF3, at: idx)
				newData.insert(0x03 & b, at: idx + 1)
				idx += 2
			} else {
				newData.insert(b, at: idx)
				idx += 1
			}
			
			if (idx >= MAX_BUFFER - 4) {
				print("too much data to stuff")
				return nil
			}
		}
		return newData
	}
	
	static func destuff(_ data: [UInt8]) -> [UInt8] {
		var newData = [UInt8]()
		var idx = 0
		
		var i = 0
		while i < data.count {
			if data[i] == 0xF3 {
				i += 1
				newData.insert(0xF0 | data[i], at: idx)
				idx += 1
			} else {
				newData.insert(data[i], at: idx)
				idx += 1
			}
			i += 1
		}
		
		return newData
	}
	
	static func extract(from data: [UInt8]) -> [UInt8]? {
		var newData = [UInt8]()
		var idx = 0
		var start = false
		
		for (i, _) in data.enumerated() {
			if start {
				if i + 1 < data.count {
					if (data[i + 1] & 0xFF) == 0xF2 {
						
						newData.insert(data[i], at: idx)
						idx += 1
						// print("breaking at pos \(i)")
						break
					} else {
						newData.insert(data[i], at: idx)
						idx += 1
					}
				} else {
					print("could not parse csafe packet")
				}
			}
			if (data[i] & 0xFF) == 0xF1 {
				start = true
			}
		}
		if !start {
			return nil
		}
		
		return newData
	}
}
