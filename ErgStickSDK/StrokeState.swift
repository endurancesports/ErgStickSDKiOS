//
//  StrokeState.swift
//  Float
//
//  Created by Nikola Milicevic on 6/20/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

@objc public enum StrokeState: Int16, Codable {
	case waitingToReachMinimumSpeed
	case waitingToAccelerate
	case driving
	case dwelling
	case recovery
	
	public var displayString: String {
		switch self {
		case .waitingToReachMinimumSpeed: return "Waiting To Reach Minimum Speed"
		case .waitingToAccelerate: return "Waiting To Accelerate"
		case .driving: return "Driving"
		case .dwelling: return "Dwelling"
		case .recovery: return "Recovery"
		}
	}
}
