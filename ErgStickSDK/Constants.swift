//
//  Constants.swift
//  ErgStick
//
//  Created by Nikola Milicevic on 12/8/17.
//  Copyright © 2017 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

struct Constants {
	
	struct Units {
		static let lbs: Float = 0.453592
	}
	
	struct PM5 {
		static let latestFirmwares = ["031", "206", "169"]
		struct InformationService {
			static let base = "CE060010-43E5-11E4-916C-0800200C9A66"
			static let hardwareVersion = "CE060013-43E5-11E4-916C-0800200C9A66"
			static let firmwareVersion = "CE060014-43E5-11E4-916C-0800200C9A66"
		}
		struct ControlPrimaryService {
			static let base = "CE060020-43E5-11E4-916C-0800200C9A66"
			static let write = "CE060021-43E5-11E4-916C-0800200C9A66"
			static let read = "CE060022-43E5-11E4-916C-0800200C9A66"
		}
		struct RowingService {
			static let base = "CE060030-43E5-11E4-916C-0800200C9A66"
			static let generalStatus = "CE060031-43E5-11E4-916C-0800200C9A66"
			static let additionalStatus1 = "CE060032-43E5-11E4-916C-0800200C9A66"
			static let additionalStatus2 = "CE060033-43E5-11E4-916C-0800200C9A66"
			static let strokeData = "CE060035-43E5-11E4-916C-0800200C9A66"
			static let additionalStrokeData = "CE060036-43E5-11E4-916C-0800200C9A66"
			static let splitIntervalData = "CE060037-43E5-11E4-916C-0800200C9A66"
			static let additionalSplitIntervalData = "CE060038-43E5-11E4-916C-0800200C9A66"
			static let endOfWorkoutSummaryData = "CE060039-43E5-11E4-916C-0800200C9A66"
			static let endOfWorkoutAdditionalSummaryData = "CE06003A-43E5-11E4-916C-0800200C9A66"
			static let forceCurve = "CE06003D-43E5-11E4-916C-0800200C9A66"
		}
	}
	
	struct ErgStick {
        // PM3 - 332, 108
        // PM4 - 332, 29
        // PM5 - 168, 30
        static let latestPMFirmwares = ["332", "108", "29", "168", "30"]
		struct InformationService {
			static let base = "CE060010-43E5-11E4-916C-0800200C9A66"
			static let serial = "CE060012-43E5-11E4-916C-0800200C9A66"
			static let hardwareVersion = "CE060013-43E5-11E4-916C-0800200C9A66"
			static let firmwareVersion = "CE060014-43E5-11E4-916C-0800200C9A66"
			static let heartRate = "CE060016-43E5-11E4-916C-0800200C9A66"
			static let heartRateMonitorId = "CE060017-43E5-11E4-916C-0800200C9A66"
		}
		struct ControlPrimaryService {
			static let base = "CE060020-43E5-11E4-916C-0800200C9A66"
			static let write = "CE060021-43E5-11E4-916C-0800200C9A66"
			static let read = "CE060022-43E5-11E4-916C-0800200C9A66"
		}
	}
	
	struct HeartRateMonitor {
		/*
		struct InformationService {
			static let base = "180A"
		}
		*/
		struct HeartRateService {
			static let base = "180D"
			static let measurement = "2A37"
//			static let bodyLocation = "2A38"
//			static let manufacturerName = "2A29"
		}
	}
}
