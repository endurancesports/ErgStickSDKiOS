//
//  CsafeCommand.swift
//  ErgStick
//
//  Created by Nikola Milicevic on 1/30/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

enum CsafeCommandType {
	case generalStatus
	case additionalStatus
	case stroke
	case splitInterval
	case forceCurve
	case pmFwRevision
}

struct CsafeCommand {
	let type: CsafeCommandType
	
	private var array: [UInt8] {
		switch type {
		case .generalStatus:
			let ROWINGSTATE: UInt8 = 0x93
			let WORKOUT_TYPE: UInt8 = 0x89
			let WORKOUT_STATE: UInt8 = 0x8D
			let WORKTIME: UInt8 = 0xA0
			let WORKDISTANCE: UInt8 = 0xA3
			let TOTAL_WORKDISTANCE: UInt8 = 0xA4
			let INTERVAL_TYPE: UInt8 = 0x8E
			let STROKE_STATE: UInt8 = 0xBF
			let DRAG_FACTOR: UInt8 = 0xC1
			let WORKOUTINTERVALCOUNT: UInt8 = 0x9F
			let GET_WORKOUTDURATION: UInt8 = 0xE8
			
			return [
				0x7E, 0x05, ROWINGSTATE, WORKOUT_TYPE, WORKOUT_STATE, WORKOUTINTERVALCOUNT, GET_WORKOUTDURATION,
				0x1A, 0x03, WORKTIME, WORKDISTANCE, INTERVAL_TYPE,
				0x7F, 0x03, STROKE_STATE, DRAG_FACTOR, TOTAL_WORKDISTANCE
			]
			
		case .additionalStatus:
			let STROKE_RATE: UInt8 = 0xB3
			let HEART_RATE: UInt8 = 0xB6
			let STROKE_500MPACE: UInt8 = 0xA8
			let TOTAL_AVG_500MPACE: UInt8 = 0xAF
			let TOTAL_AVG_POWER: UInt8 = 0xB0
			
			let TOTAL_AVG_CALORIES: UInt8 = 0xB2
			let LAST_SPLITTIME: UInt8 = 0xBA
			let LAST_SPLITDISTANCE: UInt8 = 0xBC
			let TOTAL_REST_DISTANCE: UInt8 = 0xA7
			
			return [
				0x7F, 0x9, STROKE_RATE, HEART_RATE, STROKE_500MPACE, TOTAL_AVG_500MPACE, TOTAL_AVG_POWER, TOTAL_AVG_CALORIES, LAST_SPLITTIME, LAST_SPLITDISTANCE, TOTAL_REST_DISTANCE
			]
			
		case .stroke:
			let STROKESTATS_PART1: UInt8 = 0x6E
			let STROKESTATS_PART2: UInt8 = 0x00
			
			let STROKE_POWER: UInt8 = 0xA9
			let STROKE_CALORICBURNRA: UInt8 = 0xAA
			let PROJECTED_WORKTIME: UInt8 = 0xA1
			let PROJECTED_WORKDISTANCE: UInt8 = 0xA5
			
			return [
				0x1A, 0x02, STROKESTATS_PART1, STROKESTATS_PART2,
				0x7F, 0x04, STROKE_POWER, STROKE_CALORICBURNRA, PROJECTED_WORKTIME, PROJECTED_WORKDISTANCE
			]
			
		case .splitInterval:
			let SPLIT_TIME: UInt8 = 0xB9
			let SPLIT_DISTANCE: UInt8 = 0xBB
			let SPLIT_INTERVALTYPE: UInt8 = 0x8E
			let RESTDISTANCE: UInt8 = 0xA6
			let RESTTIME: UInt8 = 0xCF
//			let WORKOUTINTERVALCOUNT: UInt8 = 0x9F
			
			// ENDING HR removed
			let SPLIT_AVG_STROKERATE: UInt8 = 0xB4
			let SPLIT_AVG_500M_PACE: UInt8 = 0xAB
			let SPLIT_AVG_CALORICBURN_RATE: UInt8 = 0xAD
			let SPLIT_AVG_CALORIES: UInt8 = 0xAE
			let SPLIT_AVG_POWER: UInt8 = 0xAC
			
			return [
				0x7F, 0x0A, SPLIT_TIME, SPLIT_DISTANCE, SPLIT_INTERVALTYPE, RESTDISTANCE, RESTTIME,
				SPLIT_AVG_STROKERATE, SPLIT_AVG_500M_PACE, SPLIT_AVG_CALORICBURN_RATE, SPLIT_AVG_POWER, SPLIT_AVG_CALORIES
			]
			
		case .forceCurve:
			let FORCEPLOTDATA: UInt8 = 0x6B
			
			return [
				0x1A, 0x03, FORCEPLOTDATA, 0x01, 0x21
			]
			
		case .pmFwRevision:
			let PM_FW_REVISION: UInt8 = 0x80
			
			return [
				0x7E, 0x01, PM_FW_REVISION
			]
		}
	}
	
	func data() -> Data? {
		guard let csafeArray = Csafe.make(from: array) else { return nil }
		return Data(csafeArray)
	}
}
