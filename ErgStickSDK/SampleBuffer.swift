//
//  SampleBuffer.swift
//  Float
//
//  Created by Nikola Milicevic on 6/20/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

struct SampleBuffer: Codable {
	let internalId: Int
	let strokeInternalId: Int
	
	let secondsPassed: Float
	
	let elapsedTime: Float
	let distance: Float
	let workoutState: WorkoutState
	let rowingState: RowingState
	let strokeState: StrokeState
	let totalWorkDistance: Float
	let workoutDuration: Float
	let workoutDistance: Float
	let dragFactor: Float
	
	let speed: Float
	let strokeRate: Int
	let currentHeartRate: Float
	let currentPace: Float
	let avgPace: Float
	let restDistance: Float
	let restTime: Float
	
	let totalAvgPower: Float
	let totalAvgCalories: Float
	let splitAvgPace: Float
	let splitAvgPower: Float
	let splitAvgCalories: Float
	let lastSplitTime: Float
	let lastSplitDistance: Float
}
