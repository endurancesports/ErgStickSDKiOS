//
//  ErgStick.swift
//  ErgStick
//
//  Created by Nikola Milicevic on 12/25/17.
//  Copyright © 2017 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation
import CoreBluetooth

class ANTPlusHRMonitor {
	var bpm: Float = 0.0
}

class ErgStick: RowingPeripheral {
	
	// MARK: - Properties
	
	var peripheral: CBPeripheral
	let services = [
		CBUUID(string: Constants.ErgStick.InformationService.base),
		CBUUID(string: Constants.ErgStick.ControlPrimaryService.base)
	]
	let characteristics = [
		CBUUID(string: Constants.ErgStick.InformationService.serial),
		CBUUID(string: Constants.ErgStick.InformationService.hardwareVersion),
		CBUUID(string: Constants.ErgStick.InformationService.firmwareVersion),
		CBUUID(string: Constants.ErgStick.InformationService.heartRate),
		CBUUID(string: Constants.ErgStick.InformationService.heartRateMonitorId),
		CBUUID(string: Constants.ErgStick.ControlPrimaryService.write),
		CBUUID(string: Constants.ErgStick.ControlPrimaryService.read)
	]
	
	var antPlusHRMonitor: ANTPlusHRMonitor?
	let workoutState = State()
	var hr = HeartRateAverage()
	
	private var commandType: CsafeCommandType = .pmFwRevision // inital value for first command, executed only once
	
	private var recorder = SessionRecorder()
	private let notifier = SessionNotifier()
	
	private var sessionBuffer = SessionBuffer()
	private var intervalBuffer = IntervalBuffer()
	private var rowingBuffer = RowingBuffer()
	private var buffer = [UInt8]()
	private var forceCurveBuffer = [Float]()
	
	private var intervalResolver: IntervalResolver?
	private var sessionResolver: SessionResolver?
	private var intervalCounter = IntervalCounter()
	
	private var firmwareVersion: String?
	
	private var writeCharacteristic: CBCharacteristic?
	private var heartRateCharacteristic: CBCharacteristic?
	private var heartRateMonitorIdCharacteristic: CBCharacteristic?
	
	private var timer = Timer()
	
	private var shouldUpdateUI = false
	
	// MARK: - Init
	
	required init(peripheral: CBPeripheral) {
		self.peripheral = peripheral
	}
	
	deinit {
		print("ErgStick died")
		timer.invalidate()
	}
	
	// MARK: - Data handling
	
	func peripheral(_ peripheral: CBPeripheral, didDiscover characteristic: CBCharacteristic) {
		if characteristic.isWrite {
			writeCharacteristic = characteristic
			sendPMFwVersionCommand()
		} else if characteristic.isRead {
			peripheral.setNotifyValue(true, for: characteristic)
		} else if characteristic.isFirmwareVersion {
			peripheral.readValue(for: characteristic)
		} else if characteristic.isHeartRate {
			heartRateCharacteristic = characteristic
		} else if characteristic.isHeartRateMonitorId {
			heartRateMonitorIdCharacteristic = characteristic
		}
	}
	
	func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic) {
		if characteristic.isFirmwareVersion {
			checkFirmwareVersion(characteristic)
			
		} else if characteristic.isRead {
			if let data = characteristic.value {
				addToBuffer(data)
			} else {
				print("no data received, send next command")
				sendCommand(to: peripheral)
			}
		} else if characteristic.isHeartRate {
			guard let data = characteristic.value, data[0] != 0 else { return }
			
			if antPlusHRMonitor == nil {
				guard let _ = heartRateMonitorIdCharacteristic else { return }
				peripheral.readValue(for: heartRateMonitorIdCharacteristic!)
			} else {
				antPlusHRMonitor?.bpm = Float(data[0])
			}
		} else if characteristic.isHeartRateMonitorId {
			antPlusHRMonitor = ANTPlusHRMonitor()
		}
	}
	
	// MARK: - ANT+ Heart rate monitor
	
	func connectToANTPlusHRMonitor() {
		guard let _ = heartRateCharacteristic, let _ = heartRateMonitorIdCharacteristic else { return }
		peripheral.setNotifyValue(true, for: heartRateCharacteristic!)
		
		peripheral.writeValue(Data([0, 0]), for: heartRateMonitorIdCharacteristic!, type: .withoutResponse)
	}
}

private extension ErgStick {
	func checkFirmwareVersion(_ characteristic: CBCharacteristic) {
		guard let data = characteristic.value, let current = data.first else {
			print("Couldn't read firmware version from ErgStick.")
			return
		}
		
		firmwareVersion = String(current)
		guard let version = PeripheralManager.shared.ergStickFirmware?.version, let latest = Float(version), Float(current) < latest else { return }
		NotificationCenter.default.post(name: .newErgStickFirmwareAvailable, object: nil)
	}
	
	func checkPMFirmwareRevision(_ version: String) {
		if !Constants.ErgStick.latestPMFirmwares.contains(version) {
			NotificationCenter.default.post(name: .newPMFirmwareAvailable, object: nil)
		}
	}
	
	func addToBuffer(_ data: Data) {
		if data.first == 0xF1 {
			// reset
			buffer = [UInt8]()
			buffer.append(contentsOf: data)
			
		} else if data.last == 0xF2 {
			buffer.append(contentsOf: data)
			populateDeviceData(from: buffer)
			
			// send next command
			sendCommand(to: peripheral)
			
		} else {
			buffer.append(contentsOf: data)
		}
	}
	
	
	func populateDeviceData(from array: [UInt8]) {
		guard let data = Csafe.get(from: array) else { return }
		
		switch commandType {
			
		case .pmFwRevision:
			guard data.count >= 20 else { return }
			
			var revision = ""
			[data[17].char, data[18].char, data[19].char].forEach { revision.append($0) }
			checkPMFirmwareRevision(revision)
			
		case .generalStatus:
			guard data.count >= 55 else { return }
			
			if let rowingState = RowingState(rawValue: Int16(data[5])) {
				rowingBuffer.rowingState = rowingState
			}
			if let workoutType = WorkoutType(rawValue: Int16(data[8])) {
				rowingBuffer.workoutType = workoutType
			}
			if let workoutState = WorkoutState(rawValue: Int16(data[11])) {
				rowingBuffer.workoutState = workoutState
			}

			rowingBuffer.intervalCount = Int(data[14])
			
			let workoutDurationType = Int(data[17])
			if workoutDurationType == 0 {
				rowingBuffer.workoutDuration = Float(with4BytesBigEndianOf: data, startingFrom: 18, multiplyBy: 0.01)
			} else if workoutDurationType == 128 {
				rowingBuffer.workoutDistance = Float(with4BytesBigEndianOf: data, startingFrom: 18, multiplyBy: 1.0)
			}
			
			if let strokeState = StrokeState(rawValue: Int16(data[45])) {
				rowingBuffer.strokeState = strokeState
			}
			
			rowingBuffer.dragFactor = Float(data[48])
			rowingBuffer.totalWorkDistance = Float(with4BytesBigEndianOf: data, startingFrom: 51, multiplyBy: 1.0)
			
			var intervalType = Int16(data[40])
			if rowingBuffer.workoutType.rawValue == WorkoutType.justRowSplits.rawValue ||
				rowingBuffer.workoutType.rawValue == WorkoutType.justRowNoSplits.rawValue ||
				rowingBuffer.workoutType.rawValue == WorkoutType.fixedDistanceSplits.rawValue ||
				rowingBuffer.workoutType.rawValue == WorkoutType.fixedDistanceNoSplits.rawValue {
				
				intervalType = IntervalType.distance.rawValue
			}
			if rowingBuffer.workoutType.rawValue == WorkoutType.fixedTimeSplits.rawValue ||
				rowingBuffer.workoutType.rawValue == WorkoutType.fixedTimeNoSplits.rawValue {
				
				intervalType = IntervalType.time.rawValue
			}
			if let intervalType = IntervalType(rawValue: intervalType) {
				rowingBuffer.intervalType = intervalType
			}
			
			// ------------------------------------------------------------------------------------------------------------
			
			setPlannedDistanceOrTime()
			
			// ------------------------------------------------------------------------------------------------------------
			
			intervalCounter.getIntervalCount(from: rowingBuffer)
			
			// ------------------------------------------------------------------------------------------------------------
			
			var elapsedTime = Float(with5BytesOf: data, startingFrom: 26, multiplyBy: 0.01)
			if intervalType == IntervalType.time.rawValue ||
				intervalType == IntervalType.timeRestUndefined.rawValue {
				
				elapsedTime = rowingBuffer.workoutDuration - elapsedTime
			}
			
			rowingBuffer.elapsedTime = elapsedTime
			
			// ------------------------------------------------------------------------------------------------------------
			
			let distance: Float
			let distanceFromCommand = Float(with5BytesOf: data, startingFrom: 33, multiplyBy: 0.1)
			if rowingBuffer.intervalType.isDistanceBased && !rowingBuffer.workoutType.isJustRow {
				distance = rowingBuffer.workoutDistance - distanceFromCommand
			} else {
				distance = distanceFromCommand
			}

			rowingBuffer.distance = distance
			
		case .additionalStatus:
			guard data.count >= 51 else { return }
			
			rowingBuffer.strokeRate = Int(data[5])
			
			if PeripheralManager.shared.isConnectedToBLEHRMonitor {
				hr.add(value: PeripheralManager.shared.heartRateMonitorBPM, workoutState: rowingBuffer.workoutState.state)
			} else if PeripheralManager.shared.isConnectedToANTPlusHRMonitor {
				hr.add(value: antPlusHRMonitor!.bpm, workoutState: rowingBuffer.workoutState.state)
			} else {
				hr.add(value: Float(data[8]), workoutState: rowingBuffer.workoutState.state)
			}
			rowingBuffer.currentHeartRate = hr.getLast()
			
			rowingBuffer.currentPace = Float(with4BytesBigEndianOf: data, startingFrom: 11, multiplyBy: 0.01)
			rowingBuffer.avgPace = Float(with4BytesBigEndianOf: data, startingFrom: 17, multiplyBy: 0.01)
			rowingBuffer.totalAvgPower = Float(with4BytesBigEndianOf: data, startingFrom: 23, multiplyBy: 1.0)
			
			rowingBuffer.totalAvgCalories = Float(with2BytesBigEndianOf: data, startingFrom: 29, multiplyBy: 1.0)
			rowingBuffer.lastSplitTime = Float(with4BytesBigEndianOf: data, startingFrom: 35, multiplyBy: 0.01)
			rowingBuffer.splitIntervalTime = Float(with4BytesBigEndianOf: data, startingFrom: 35, multiplyBy: 0.01)
			rowingBuffer.lastSplitDistance = Float(with4BytesBigEndianOf: data, startingFrom: 41, multiplyBy: 1.0)
			rowingBuffer.splitIntervalDistance = Float(with4BytesBigEndianOf: data, startingFrom: 41, multiplyBy: 1.0)
			
			rowingBuffer.restDistance = Float(with4BytesBigEndianOf: data, startingFrom: 47, multiplyBy: 1.0)
			
			if rowingBuffer.currentPace > 0 {
				rowingBuffer.speed = 500.0 / rowingBuffer.currentPace
			}
			
		case .stroke:
			guard data.count >= 47 else { return }
			
			rowingBuffer.strokeDistance = Float(with2BytesOf: data, startingFrom: 5, multiplyBy: 0.01)
			rowingBuffer.driveTime = Float(data[7]) * 0.01
			rowingBuffer.strokeRecoveryTime = Float(with2BytesOf: data, startingFrom: 8, multiplyBy: 0.01)
			rowingBuffer.driveLength = Float(data[10]) * 0.01
			rowingBuffer.strokeCount = Int(Float(with2BytesOf: data, startingFrom: 11, multiplyBy: 1.0))
			rowingBuffer.peakDriveForce = Float(with2BytesOf: data, startingFrom: 13, multiplyBy: 0.01)
			rowingBuffer.avgDriveForce = Float(with2BytesOf: data, startingFrom: 17, multiplyBy: 0.01)
			rowingBuffer.workPerStroke = Float(with2BytesOf: data, startingFrom: 19, multiplyBy: 0.01)
			
			rowingBuffer.strokePower = Float(with4BytesBigEndianOf: data, startingFrom: 25, multiplyBy: 1.0)
			rowingBuffer.strokeCalories = Float(with4BytesBigEndianOf: data, startingFrom: 31, multiplyBy: 1.0)
			rowingBuffer.projectedWorkTime = Float(with4BytesBigEndianOf: data, startingFrom: 37, multiplyBy: 0.01)
			rowingBuffer.projectedWorkDistance = Float(with4BytesBigEndianOf: data, startingFrom: 43, multiplyBy: 1.0)
			
		case .splitInterval:
			guard data.count >= 55 else { return }
			
			rowingBuffer.restTime = Float(with4BytesBigEndianOf: data, startingFrom: 24, multiplyBy: 0.01)
			rowingBuffer.restDistance = Float(with2BytesBigEndianOf: data, startingFrom: 20, multiplyBy: 1.0)
			rowingBuffer.splitIntervalAverageStrokeRate = Float(data[30])
			rowingBuffer.splitAvgPace = Float(with4BytesBigEndianOf: data, startingFrom: 33, multiplyBy: 0.01)
			rowingBuffer.splitIntervalAveragePace = Float(with4BytesBigEndianOf: data, startingFrom: 33, multiplyBy: 0.01)
			rowingBuffer.splitIntervalAverageCalories = Float(with4BytesBigEndianOf: data, startingFrom: 39, multiplyBy: 0.01)
			rowingBuffer.splitAvgPower = Float(with4BytesBigEndianOf: data, startingFrom: 45, multiplyBy: 1.0)
			rowingBuffer.splitIntervalTotalCalories = Float(with4BytesBigEndianOf: data, startingFrom: 51, multiplyBy: 1.0)
			
			if rowingBuffer.splitAvgPace > 0 {
				rowingBuffer.splitIntervalSpeed = 500.0 / rowingBuffer.splitAvgPace
			}
			
			// ------------------------------------------------------------------------------------------------------------
			
			workoutState.setState(rowingBuffer.workoutState.state)
			
			resetUIIfNeeded()
			
			if workoutState.isRunning || workoutState.hasFinished {
				notifier.notify(rowingBuffer)
			}
			
			intervalResolver?.feed(rowingBuffer)
			sessionResolver?.feed(rowingBuffer)
			
			if workoutState.isRunning && intervalCounter.hasStartedNextInterval {
				recordInterval(isCompleted: true)
			}
			
			if workoutState.isRunning {
				recorder.record(rowingBuffer)
			}
			
			if workoutState.hasChangedFromRunning {
				if workoutState.hasFinished {
					print("hasFinished, save")
					endWorkout(isCompleted: true)
				} else {
					print("stopped")
					
					if rowingBuffer.workoutType.isJustRow && !workoutState.isWorkoutTypeChanged {
						if rowingBuffer.distance < 250 {
							print("just row, don't save")
							break
						} else {
							print("just row, save")
							endWorkout(isCompleted: true)
							return
						}
					}
					
					print("save incomplete workout")
					endWorkout(isCompleted: false)
				}
			}
			
			// placed last because undefined rest intervals magically turn to variabale intervals upon workout logged
			createResolversIfNeeded()
			
			// ------------------------------------------------------------------------------------------------------------
			
		case .forceCurve:
			var i = 6
            while ((i < 6 + data[5]) && (i + 1 < data.count)) {
				let temp = Float(with2BytesOf: data, startingFrom: i, multiplyBy: 1)
				forceCurveBuffer.append(temp)
				i += 2
			}
			
			if rowingBuffer.strokeState.rawValue == 4 && shouldUpdateUI == true {
				rowingBuffer.plot = forceCurveBuffer
				
				// UI
				notifier.notify(rowingBuffer.plot)
				shouldUpdateUI = false
				
				// reset force curve buffer
				forceCurveBuffer = [Float]()
			}
		}
	}

	func sendCommand(to peripheral: CBPeripheral) {
		nextCommand()
		
		if commandType == .forceCurve {
			if rowingBuffer.strokeState.rawValue == 2 || rowingBuffer.strokeState.rawValue == 3 {
				shouldUpdateUI = true
			}
		}
		
		guard let command = CsafeCommand(type: commandType).data(), let c = writeCharacteristic else { return }
        DispatchQueue.global(qos: .background).asyncAfter(deadline: .now() + 0.075) {
            peripheral.writeValue(command, for: c, type: peripheral.canSendWriteWithoutResponse ? .withoutResponse: .withResponse)
        }
		
		
		// stuck communication bug fix
		timer.invalidate()
        timer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { _ in
            print("communication stuck, sending command again")
            peripheral.writeValue(command, for: c, type: peripheral.canSendWriteWithoutResponse ? .withoutResponse: .withResponse)
        }
	}
	
	func sendPMFwVersionCommand() {
		guard let command = CsafeCommand(type: commandType).data(), let c = writeCharacteristic else { return }
		peripheral.writeValue(command, for: c, type: .withResponse)
	}
	
	func nextCommand() {
		switch commandType {
		case .generalStatus:
			commandType = .additionalStatus
		case .additionalStatus:
			commandType = .stroke
		case .stroke:
			commandType = .splitInterval
		case .splitInterval:
			commandType = .forceCurve
		case .forceCurve:
			commandType = .generalStatus
			
		// executed only once
		case .pmFwRevision:
			commandType = .generalStatus
		}
	}
	
	func setPlannedDistanceOrTime() {
		if rowingBuffer.intervalType.isWorkInterval {
			if rowingBuffer.intervalType.isDistanceBased {
				intervalBuffer.plannedDistanceOrTime = rowingBuffer.workoutDistance
			} else {
				intervalBuffer.plannedDistanceOrTime = rowingBuffer.workoutDuration
			}
		}
	}
	
	func recordInterval(isCompleted: Bool) {
		intervalBuffer.avgHeartRate = hr.getSplitAverage()
		
		if isCompleted {
			intervalResolver?.resolve(intervalBuffer)
			print(#function, "resolve interval")
		} else {
			intervalResolver?.resolveIncomplete(intervalBuffer)
			print(#function, "resolve incomplete interval")
		}
		
		recorder.record(intervalBuffer)
	}
	
	func populateSessionBuffer() {
		sessionBuffer.deviceIdentifier = peripheral.name
		sessionBuffer.deviceFirmware = firmwareVersion
		sessionBuffer.workoutType = rowingBuffer.workoutType
	}
	
	func getHeartRateData() {
		sessionBuffer.endingHeartRate = hr.getLast()
		sessionBuffer.avgHeartRate = hr.getAverage()
		sessionBuffer.minHeartRate = hr.getMin()
		sessionBuffer.maxHeartRate = hr.getMax()
	}
	
	func endWorkout(isCompleted: Bool) {
		print(#function)
		recordInterval(isCompleted: isCompleted)
		getHeartRateData()
		sessionResolver?.resolve(sessionBuffer)
		recorder.recordErgstick(sessionBuffer)
		
		resetBuffer()
		resetResolvers()
	}
	
	func createResolversIfNeeded() {
		switch rowingBuffer.workoutType {
		case .fixedTimeIntervals:
			
			if intervalResolver == nil ||
				!(intervalResolver is TimeIntervalResolver) {
				
				intervalResolver = TimeIntervalResolver()
				sessionResolver = TimeIntervalSessionResolver()
				
				print("created TimeIntervalResolver")
				// ------------------------------------------------------------------------------------------------------------
				populateSessionBuffer()
				// ------------------------------------------------------------------------------------------------------------
			}
			
		case .variableIntervals:
			
			if intervalResolver == nil ||
				!(intervalResolver is VariableIntervalsResolver) {
				
				intervalResolver = VariableIntervalsResolver()
				sessionResolver = VariableIntervalsSessionResolver()
				
				print("created VariableIntervalsResolver")
				// ------------------------------------------------------------------------------------------------------------
				populateSessionBuffer()
				// ------------------------------------------------------------------------------------------------------------
			}
			
		case .fixedTimeSplits:
			
			if intervalResolver == nil ||
				!(intervalResolver is SingleTimeResolver) {
				
				intervalResolver = SingleTimeResolver()
				sessionResolver = SingleTimeSessionResolver()
			
				print("created SingleTimeResolver")
				// ------------------------------------------------------------------------------------------------------------
				populateSessionBuffer()
				// ------------------------------------------------------------------------------------------------------------
			}
			
		case .fixedDistanceIntervals:
			
			if intervalResolver == nil ||
				!(intervalResolver is DistanceIntervalResolver) {
				
				intervalResolver = DistanceIntervalResolver()
				sessionResolver = DistanceIntervalsSessionResolver()
			
				print("created DistanceIntervalResolver")
				// ------------------------------------------------------------------------------------------------------------
				populateSessionBuffer()
				// ------------------------------------------------------------------------------------------------------------
			}
			
		case .variableIntervalsRestUndefined:
			
			if intervalResolver == nil ||
				!(intervalResolver is VariableIntervalsRestUndefinedResolver) {
				
				intervalResolver = VariableIntervalsRestUndefinedResolver()
				sessionResolver = VariableIntervalsRestUndefinedSessionResolver()
	
				print("created VariableIntervalsRestUndefinedResolver")
				// ------------------------------------------------------------------------------------------------------------
				populateSessionBuffer()
				// ------------------------------------------------------------------------------------------------------------
			}
			
		case .fixedDistanceSplits:
			
			if intervalResolver == nil ||
				!(intervalResolver is SingleDistanceResolver) {
				
				intervalResolver = SingleDistanceResolver()
				sessionResolver = SingleDistanceSessionResolver()
	
				print("created SingleDistanceResolver")
				// ------------------------------------------------------------------------------------------------------------
				populateSessionBuffer()
				// ------------------------------------------------------------------------------------------------------------
			}
			
		case .justRowSplits:
			
			if intervalResolver == nil ||
				!(intervalResolver is JustRowResolver) {
				
				intervalResolver = JustRowResolver()
				sessionResolver = JustRowSessionResolver()
			
				print("created JustRowResolver")
				// ------------------------------------------------------------------------------------------------------------
				populateSessionBuffer()
				// ------------------------------------------------------------------------------------------------------------
				
				// good time to reset
				resetBuffer()
			}
			
		default:
			break
		}
	}
	
	func resetUIIfNeeded() {
		workoutState.setWorkoutType(rowingBuffer.workoutType)
		workoutState.setIntervalType(rowingBuffer.intervalType)
		
		if rowingBuffer.workoutType.isDistanceBased {
			workoutState.setWorkoutDistanceOrTime(rowingBuffer.workoutDistance)
		} else {
			workoutState.setWorkoutDistanceOrTime(rowingBuffer.workoutDuration)
		}
		
		// workout end
		if workoutState.hasChangedToWaitToBegin {
			notifier.notifyReset(rowingBuffer, toZero: true)
			
			DispatchQueue.main.asyncAfter(deadline: .now() + 2) { [weak self] in
				self?.resetBuffer()
			}
		
		// workout type / interval type changed
		} else if (workoutState.isWorkoutTypeChanged || workoutState.isIntervalTypeChanged) {
			notifier.notifyReset(rowingBuffer)
			
		// interval based workout
		} else if rowingBuffer.workoutType.isIntervalBased {
			if workoutState.isDistanceOrTimeChanged {
				notifier.notifyReset(rowingBuffer)
			}
		}
	}
	
	func resetBuffer() {
		print(#function)
		
		hr = HeartRateAverage()
		recorder = SessionRecorder()
		rowingBuffer = RowingBuffer()
		intervalCounter = IntervalCounter()
		timer.invalidate()
	}
	
	func resetResolvers() {
		intervalResolver = nil
		sessionResolver = nil
	}
}
