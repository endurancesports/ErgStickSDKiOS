//
//  ErgStickFirmwareUpdater.swift
//  Float
//
//  Created by Nikola Milicevic on 6/20/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation
import CoreBluetooth
import iOSDFULibrary

public protocol ErgStickFirmwareUpdaterDelegate: class {
	func didFinishDownloading(to location: URL)
	func didUpdateProgress(progress: Float)
	func didFinishDFU(success: Bool)
}

// MARK: - Types

struct ErgStickFirmwaresResponse: Decodable {
	let data: [ErgStickFirmware]
}

struct ErgStickFirmware: Decodable {
	let version: String
	let fileUrl: String
}

public class ErgStickFirmwareUpdater: NSObject, URLSessionDelegate {
	
	// MARK: - Properties
	
	public weak var delegate: ErgStickFirmwareUpdaterDelegate?
	
	private lazy var manager = CBCentralManager(delegate: self, queue: nil)
	private var dfuPeripheral: CBPeripheral? {
		didSet {
			print(#function)
			if let _ = dfuPeripheral {
				manager.connect(dfuPeripheral!, options: nil)
			}
		}
	}
	
	private var dfuController: DFUServiceController?
	private var firmware: DFUFirmware?
	private var firmwareLocation: URL?

	public var isConnectedToPeripheral: Bool {
		return dfuPeripheral != nil
	}

	// MARK: - Check for update
	
	public class func checkForUpdate() {
		guard let url = URL(string: "https://api-dev.ergstick.com/v3/firmwares") else { return }
		let request = URLRequest(url: url)
		
		URLSession.shared.dataTask(with: request) { (data, response, error) in
			DispatchQueue.main.async {
				
				guard let data = data else { return }
				
				let decoder = JSONDecoder()
				decoder.keyDecodingStrategy = .convertFromSnakeCase
				do {
					let firmwareResponse = try decoder.decode(ErgStickFirmwaresResponse.self, from: data)
					if let latest = firmwareResponse.data.sorted (by: { $0.version > $1.version }).first {
						PeripheralManager.shared.ergStickFirmware = latest
					}
				} catch {
					print(error)
				}
			}
		}.resume()
	}
	
	public func downloadUpdate() {
		guard let fileUrl = PeripheralManager.shared.ergStickFirmware?.fileUrl, let url = URL(string: fileUrl) else { return }
		let request = URLRequest(url: url)
		let config = URLSessionConfiguration.default
		let session = URLSession(configuration: config, delegate: self, delegateQueue: OperationQueue.main)
		session.downloadTask(with: request).resume()
	}
	
	// MARK: - Firmware update
	
	public func startUpdatingFirmware(_ firmwareLocation: URL) {
		self.firmwareLocation = firmwareLocation
		print(manager)
	}
	
	deinit {
        if manager.state == .poweredOn {
            manager.stopScan()
        }
	}
}

extension ErgStickFirmwareUpdater: URLSessionDownloadDelegate {
	public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didFinishDownloadingTo location: URL) {
		delegate?.didFinishDownloading(to: location)
	}
	
	public func urlSession(_ session: URLSession, downloadTask: URLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten: Int64, totalBytesExpectedToWrite: Int64) {
		let progress = Float(totalBytesWritten) / Float(totalBytesExpectedToWrite)
		print(#function, bytesWritten, totalBytesWritten, totalBytesExpectedToWrite, progress)
		
		// progress should be used instead, but sometimes some funky numbers are given in totalBytesWritten and totalBytesExpectedToWrite, so progress is unusable
		delegate?.didUpdateProgress(progress: 1)
	}
}

extension ErgStickFirmwareUpdater: CBCentralManagerDelegate {
	public func centralManagerDidUpdateState(_ central: CBCentralManager) {
		if central.state == .poweredOn {
			manager.scanForPeripherals(withServices: nil, options: nil)
		}
	}
	
	public func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
		if peripheral.name == "DfuTarg" {
			dfuPeripheral = peripheral
			central.stopScan()
		}
	}
	
	public func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
		let name = peripheral.name ?? "Unknown"
		print("Connected to peripheral: \(name)")
		
		guard let firmwareLocation = firmwareLocation else { return }
		firmware = DFUFirmware(urlToZipFile: firmwareLocation)
		startDFUProcess()
	}
	
	public func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
		let name = peripheral.name ?? "Unknown"
		print("Disconnected from peripheral: \(name)")
	}
}

extension ErgStickFirmwareUpdater: DFUServiceDelegate {
	public func dfuStateDidChange(to state: DFUState) {
		print("Changed state to: \(state.description())")
		if state == .completed {
			delegate?.didFinishDFU(success: true)
			// Forget the controller when DFU is done
			dfuController = nil
		}
		
	}
	
	public func dfuError(_ error: DFUError, didOccurWithMessage message: String) {
		print("Error \(error.rawValue): \(message)")
		delegate?.didFinishDFU(success: false)
		// Forget the controller when DFU finished with an error
		dfuController = nil
	}
}

extension ErgStickFirmwareUpdater: DFUProgressDelegate {
	public func dfuProgressDidChange(for part: Int, outOf totalParts: Int, to progress: Int, currentSpeedBytesPerSecond: Double, avgSpeedBytesPerSecond: Double) {
		print("part: \(part), totalParts: \(totalParts), progress: \(progress)")
		delegate?.didUpdateProgress(progress: Float(progress)/100.0)
	}
}

extension ErgStickFirmwareUpdater: LoggerDelegate {
	public func logWith(_ level: LogLevel, message: String) {
		print("\(level.name()): \(message)")
	}
}

private extension ErgStickFirmwareUpdater {
	func startDFUProcess() {
		print(#function)
		guard let _ = dfuPeripheral else { return }
		
		let dfuInitiator = DFUServiceInitiator(centralManager: manager, target: dfuPeripheral!)
		dfuInitiator.delegate = self
		dfuInitiator.progressDelegate = self
		dfuInitiator.logger = self
		
		dfuController = dfuInitiator.with(firmware: firmware!).start()
	}
}
