//
//  PM5.swift
//  ErgStick
//
//  Created by Nikola Milicevic on 12/11/17.
//  Copyright © 2017 Endurance Sports Research Ltd. All rights reserved.
//

import CoreBluetooth

class PM5: RowingPeripheral {
	
	// MARK: - Properties
	
	var peripheral: CBPeripheral
	let services = [
		CBUUID(string: Constants.PM5.InformationService.base),
		CBUUID(string: Constants.PM5.ControlPrimaryService.base),
		CBUUID(string: Constants.PM5.RowingService.base)
	]
	let characteristics = [
		CBUUID(string: Constants.PM5.InformationService.hardwareVersion),
		CBUUID(string: Constants.PM5.InformationService.firmwareVersion),
		CBUUID(string: Constants.PM5.ControlPrimaryService.write),
		CBUUID(string: Constants.PM5.ControlPrimaryService.read),
		CBUUID(string: Constants.PM5.RowingService.generalStatus),
		CBUUID(string: Constants.PM5.RowingService.additionalStatus1),
		CBUUID(string: Constants.PM5.RowingService.additionalStatus2),
		CBUUID(string: Constants.PM5.RowingService.strokeData),
		CBUUID(string: Constants.PM5.RowingService.additionalStrokeData),
		CBUUID(string: Constants.PM5.RowingService.splitIntervalData),
		CBUUID(string: Constants.PM5.RowingService.additionalSplitIntervalData),
		CBUUID(string: Constants.PM5.RowingService.endOfWorkoutSummaryData),
		CBUUID(string: Constants.PM5.RowingService.endOfWorkoutAdditionalSummaryData),
		
		// TODO: uncomment when Concept2 fixes newest PM5 monitor crash (hw revision > 600)
		CBUUID(string: Constants.PM5.RowingService.forceCurve)
	]
	
	let workoutState = State()
	var hr = HeartRateAverage()
	
	private var recorder = SessionRecorder()
	private let notifier = SessionNotifier()
	
	private var sessionBuffer = SessionBuffer()
	private var intervalBuffer = IntervalBuffer()
	private var rowingBuffer = RowingBuffer()
	private var bytesBuffer = [UInt8]()
	private var forceCurveBuffer = [Float]()
	
	private var hardwareVersion: String?
	private var firmwareVersion: String?
	private var writeCharacteristic: CBCharacteristic?
	
	private var shouldUpdateUI = false
	
	// MARK: - Init
	
	required init(peripheral: CBPeripheral) {
		self.peripheral = peripheral
	}
	
	// MARK: - Data handling
	
	func peripheral(_ peripheral: CBPeripheral, didDiscover characteristic: CBCharacteristic) {
		// discover hardware version first
		guard let hardwareVersion = hardwareVersion else {
			if characteristic.isHardwareVersion {
				peripheral.readValue(for: characteristic)
			}
			return
		}
		
		if characteristic.isFirmwareVersion {
			peripheral.readValue(for: characteristic)
			
		} else if characteristic.isWrite {
			guard let hw = Int(hardwareVersion), hw < 600 else { return }
			writeCharacteristic = characteristic
			
		} else if characteristic.isRead {
			guard let hw = Int(hardwareVersion), hw < 600 else { return }
			peripheral.setNotifyValue(true, for: characteristic)
			
		} else {
			peripheral.setNotifyValue(true, for: characteristic)
		}
	}
	
	func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic) {
		if characteristic.isHardwareVersion {
			guard let data = characteristic.value, let hw = String(data: data, encoding: .utf8) else { return }
			
			hardwareVersion = hw
			
			// discover services again (and react based on hardware version)
			peripheral.discoverServices(services)
			
		} else if characteristic.isFirmwareVersion {
			guard let data = characteristic.value, data.count >= 14 else { return }
			
			var fw = ""
			[data[12].char, data[13].char, data[14].char].forEach { fw.append($0) }
			firmwareVersion = fw
			
			checkFirmwareRevision(fw)

		} else if characteristic.isRead {
			guard let data = characteristic.value else { return }
			addToBuffer(data)
			
		} else {
			parse(characteristic)
		}
	}
}

private extension PM5 {
	func checkFirmwareRevision(_ version: String) {
		if !Constants.PM5.latestFirmwares.contains(version) {
			NotificationCenter.default.post(name: .newPM5FirmwareAvailable, object: nil)
		}
	}

	func addToBuffer(_ data: Data) {
		if data.first == 0xF1 {
			// reset
			bytesBuffer = [UInt8]()
			bytesBuffer.append(contentsOf: data)
			
		} else if data.last == 0xF2 {
			bytesBuffer.append(contentsOf: data)
			parseForceCurve(from: bytesBuffer)
			
		} else {
			bytesBuffer.append(contentsOf: data)
		}
	}
	
	func parseForceCurve(from array: [UInt8]) {
		guard let data = Csafe.get(from: array) else { return }
		
		/*
		index 3 -> command (6b)
		index 4 -> total bytes
		index 5 -> useful bytes (number of bytes to parse)
		index 6 -> start of response
		
		09 1a 23 6b 21 20 00 00 3b 00 3b 00 43 00 43 00 47 00 4c 00 4c 00 50 00 56 00 56 00 5e 00 5e 00 69 00 6f 00 6f 00 24
		89 1a 23 6b 21 20 77 00 78 00 7b 00 7b 00 78 00 7a 00 7a 00 77 00 74 00 6f 00 6f 00 69 00 61 00 59 00 59 00 4b 00 ed
		09 1a 23 6b 21 14 4c 00 42 00 33 00 33 00 15 00 02 00 01 00 01 00 00 00 00 00 00 6f 00 69 00 61 00 59 00 59 00 4b 5b
		*/
		
		var i = 6
		while i < 6 + data[5] {
			let temp = Float(with2BytesOf: data, startingFrom: i, multiplyBy: 1)
			forceCurveBuffer.append(temp)
			i += 2
		}
		
		if rowingBuffer.strokeState.rawValue == 4 && shouldUpdateUI == true {
			rowingBuffer.plot = forceCurveBuffer
			
			// UI
			notifier.notify(rowingBuffer.plot)
			shouldUpdateUI = false
			
			// reset force curve buffer
			forceCurveBuffer = [Float]()
		}
	}
	
	func sendForceCurveCommandIfNeeded() {
		guard let hw = Int(hardwareVersion!), hw < 600 else { return }
		
		if rowingBuffer.strokeState.rawValue == 2 || rowingBuffer.strokeState.rawValue == 3 {
			sendCommand(to: peripheral)
			shouldUpdateUI = true
		}
	}
	
	func parse(_ characteristic: CBCharacteristic) {
		guard let data = characteristic.value else { return }
		
		sendForceCurveCommandIfNeeded()
		
		if characteristic.isGeneralStatus {
			rowingBuffer.elapsedTime = Float(with3BytesOf: data, startingFrom: 0, multiplyBy: 0.01)
			rowingBuffer.distance = Float(with3BytesOf: data, startingFrom: 3, multiplyBy: 0.1)
			
			if let workoutType = WorkoutType(rawValue: Int16(data[6])) {
				rowingBuffer.workoutType = workoutType
			}
			if let intervalType = IntervalType(rawValue: Int16(data[7])) {
				rowingBuffer.intervalType = intervalType
			}
			if let workoutState = WorkoutState(rawValue: Int16(data[8])) {
				rowingBuffer.workoutState = workoutState
			}
			if let rowingState = RowingState(rawValue: Int16(data[9])) {
				rowingBuffer.rowingState = rowingState
			}
			if let strokeState = StrokeState(rawValue: Int16(data[10])) {
				rowingBuffer.strokeState = strokeState
			}
			
			rowingBuffer.totalWorkDistance = Float(with3BytesOf: data, startingFrom: 11, multiplyBy: 0.1)
			rowingBuffer.workoutDuration = Float(with3BytesOf: data, startingFrom: 14, multiplyBy: 0.01)
			rowingBuffer.workoutDistance = rowingBuffer.workoutDuration * 100.0
			rowingBuffer.dragFactor = Float(data[18])
			
			workoutState.setState(rowingBuffer.workoutState.state)
			workoutState.setWorkoutType(rowingBuffer.workoutType)
			workoutState.setIntervalType(rowingBuffer.intervalType)
			
			if workoutState.isWaitToBegin {
				resetUIIfNeeded()
			}
			
		} else if characteristic.isAdditionalStatus1 {
			rowingBuffer.speed = Float(with2BytesOf: data, startingFrom: 3, multiplyBy: 0.001)
			rowingBuffer.strokeRate = Int(data[5])
			
			if PeripheralManager.shared.isConnectedToBLEHRMonitor {
				hr.add(value: PeripheralManager.shared.heartRateMonitorBPM, workoutState: rowingBuffer.workoutState.state)
			} else {
				if Float(data[6]) != 255 {
					hr.add(value: Float(data[6]), workoutState: rowingBuffer.workoutState.state)
				}
			}
			rowingBuffer.currentHeartRate = hr.getLast()
			
			rowingBuffer.currentPace = Float(with2BytesOf: data, startingFrom: 7, multiplyBy: 0.01)
			rowingBuffer.avgPace = Float(with2BytesOf: data, startingFrom: 9, multiplyBy: 0.01)
			rowingBuffer.restDistance = Float(with2BytesOf: data, startingFrom: 11, multiplyBy: 0.1)
			rowingBuffer.restTime = Float(with3BytesOf: data, startingFrom: 13, multiplyBy: 0.01)
			
		} else if characteristic.isAdditionalStatus2 {
			rowingBuffer.intervalCount = Int(data[3])
			rowingBuffer.totalAvgPower = Float(with2BytesOf: data, startingFrom: 4, multiplyBy: 1.0)
			rowingBuffer.totalAvgCalories = Float(with2BytesOf: data, startingFrom: 6, multiplyBy: 1.0)
			rowingBuffer.splitAvgPace = Float(with2BytesOf: data, startingFrom: 8, multiplyBy: 0.01)
			rowingBuffer.splitAvgPower = Float(with2BytesOf: data, startingFrom: 10, multiplyBy: 1.0)
			rowingBuffer.splitAvgCalories = Float(with2BytesOf: data, startingFrom: 12, multiplyBy: 1.0)
			rowingBuffer.lastSplitTime = Float(with3BytesOf: data, startingFrom: 14, multiplyBy: 0.1)
			rowingBuffer.lastSplitDistance = Float(with2BytesOf: data, startingFrom: 17, multiplyBy: 1.0)
			
		} else if characteristic.isStrokeData {
			rowingBuffer.driveLength = Float(data[6]) * 0.01
			rowingBuffer.driveTime = Float(data[7]) * 0.01
			rowingBuffer.strokeRecoveryTime = Float(with2BytesOf: data, startingFrom: 8, multiplyBy: 0.01)
			rowingBuffer.strokeDistance = Float(with2BytesOf: data, startingFrom: 10, multiplyBy: 0.01)
			rowingBuffer.peakDriveForce = Float(with2BytesOf: data, startingFrom: 12, multiplyBy: 0.1)
			rowingBuffer.avgDriveForce = Float(with2BytesOf: data, startingFrom: 14, multiplyBy: 0.1)
			rowingBuffer.workPerStroke = Float(with2BytesOf: data, startingFrom: 16, multiplyBy: 0.1)
			rowingBuffer.strokeCount = Int(Float(with2BytesOf: data, startingFrom: 18, multiplyBy: 1.0))
			
		} else if characteristic.isAdditionalStrokeData {
			rowingBuffer.strokePower = Float(with2BytesOf: data, startingFrom: 3, multiplyBy: 1.0)
			rowingBuffer.strokeCalories = Float(with2BytesOf: data, startingFrom: 5, multiplyBy: 1.0)
			rowingBuffer.projectedWorkTime = Float(with3BytesOf: data, startingFrom: 9, multiplyBy: 1.0)
			rowingBuffer.projectedWorkDistance = Float(with3BytesOf: data, startingFrom: 12, multiplyBy: 1.0)
			
		} else if characteristic.isForceCurve {
			// TODO: uncomment when Concept2 fixes newest PM5 monitor crash (hw revision > 600)
			let numberOfChunks = data[0] >> 4
			let chunkIndex = data[1]
			
            if numberOfChunks > 0 {
                var i = 2
                while i < data.count {
                    let temp = Float(with2BytesOf: data, startingFrom: i, multiplyBy: 1)
                    forceCurveBuffer.append(temp)
                    i += 2
                }
                
                // if last chunk
                if chunkIndex == numberOfChunks - 1 {
                    rowingBuffer.plot = forceCurveBuffer
                    
                    // UI
                    notifier.notify(rowingBuffer.plot)
                    shouldUpdateUI = false
                    
                    // reset force curve buffer
                    forceCurveBuffer = [Float]()
                }
            }
			
		} else if characteristic.isSplitInterval {
			intervalBuffer.elapsedTime = Float(with3BytesOf: data, startingFrom: 6, multiplyBy: 0.1)
			intervalBuffer.elapsedDistance = Float(with3BytesOf: data, startingFrom: 9, multiplyBy: 1.0)
			intervalBuffer.restTime = Float(with2BytesOf: data, startingFrom: 12, multiplyBy: 1.0)
			intervalBuffer.restDistance = Float(with2BytesOf: data, startingFrom: 14, multiplyBy: 1.0)
            intervalBuffer.intervalType = IntervalType(rawValue: Int16(data[16]))!
			
		} else if characteristic.isAdditionalSplitInterval {
			intervalBuffer.avgStrokeRate = Float(data[3])
			intervalBuffer.avgHeartRate = hr.getSplitAverage()
			intervalBuffer.avgPace = Float(with2BytesOf: data, startingFrom: 6, multiplyBy: 0.1)
			intervalBuffer.totalCalories = Float(with2BytesOf: data, startingFrom: 8, multiplyBy: 1.0)
			intervalBuffer.avgCalories = Float(with2BytesOf: data, startingFrom: 10, multiplyBy: 1.0)
			intervalBuffer.avgSpeed = Float(with2BytesOf: data, startingFrom: 12, multiplyBy: 0.001)
			intervalBuffer.avgPower = Float(with2BytesOf: data, startingFrom: 14, multiplyBy: 1.0)
			intervalBuffer.avgDragFactor = Float(data[16])
			
            if let intervalType = intervalBuffer.intervalType {
                if intervalType.isDistanceBased {
                    intervalBuffer.plannedDistanceOrTime = rowingBuffer.workoutDistance
                } else {
                    intervalBuffer.plannedDistanceOrTime = rowingBuffer.workoutDuration
                }
            }
			
			recorder.record(intervalBuffer)
			
		} else if characteristic.isEndOfWorkout {
			sessionBuffer.elapsedTime = Float(with3BytesOf: data, startingFrom: 4, multiplyBy: 0.01)
			sessionBuffer.distance = Float(with3BytesOf: data, startingFrom: 7, multiplyBy: 0.1)
			sessionBuffer.avgStrokeRate = Float(data[10])
			sessionBuffer.endingHeartRate = hr.getLast()
			sessionBuffer.avgHeartRate = hr.getAverage()
			sessionBuffer.minHeartRate = hr.getMin()
			sessionBuffer.maxHeartRate = hr.getMax()
			sessionBuffer.dragFactorAvg = Float(data[15])
			sessionBuffer.avgPace = Float(with2BytesOf: data, startingFrom: 18, multiplyBy: 0.1)
			
		} else if characteristic.isEndOfWorkoutAdditional {
			sessionBuffer.avgPower = Float(with2BytesOf: data, startingFrom: 10, multiplyBy: 1.0)
			
			sessionBuffer.deviceIdentifier = peripheral.name
			sessionBuffer.deviceFirmware = firmwareVersion
			sessionBuffer.workoutType = workoutState.getCurrentWorkoutType()
			
			notifier.notify(rowingBuffer)
			recorder.record(sessionBuffer)
			reset()
		}
		
		if workoutState.isRunning {
			notifier.notify(rowingBuffer)
			
			if rowingBuffer.workoutType.isJustRow && workoutState.getPreviousState() == .waitToBegin && rowingBuffer.intervalCount != 0 {
				rowingBuffer.intervalCount = 0
			}
			
			if characteristic.isGeneralStatus {
				recorder.record(rowingBuffer)
			}
			
		} else if rowingBuffer.workoutType.isJustRow && workoutState.hasTerminatedJustRow {
			reset()
		}
	}
	
	func sendCommand(to peripheral: CBPeripheral) {
		guard let command = CsafeCommand(type: .forceCurve).data(), let c = writeCharacteristic else { return }
		peripheral.writeValue(command, for: c, type: .withResponse)
	}
	
	func resetUIIfNeeded() {
		if rowingBuffer.workoutType.isJustRow && workoutState.isWorkoutJustRow && workoutState.hasTerminatedJustRow {
			notifier.notifyReset(rowingBuffer)
		} else {
			if rowingBuffer.workoutType.isDistanceBased {
				workoutState.setWorkoutDistanceOrTime(rowingBuffer.workoutDistance)
			} else {
				workoutState.setWorkoutDistanceOrTime(rowingBuffer.workoutDuration)
			}
			if workoutState.isDistanceOrTimeChanged {
				notifier.notifyReset(rowingBuffer)
			}
		}
	}
	
	func reset() {
		hr = HeartRateAverage()
		recorder = SessionRecorder()
		sessionBuffer = SessionBuffer()
		intervalBuffer = IntervalBuffer()
		rowingBuffer = RowingBuffer()
	}
}
