//
//  IntervalType.swift
//  Float
//
//  Created by Nikola Milicevic on 6/20/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

@objc public enum IntervalType: Int16, Codable {
	case time = 0
	case distance
	case rest
	case timeRestUndefined
	case distanceRestUndefined
	case restUndefined
	case calories
	case caloriesRestUndefined
	case wattMinutes
	case wattMinutesRestUndefined
	case none = 255
	
	public var isWorkInterval: Bool {
		return !isRestInterval
	}
	public var isRestInterval: Bool {
		return self == .rest || self == .restUndefined
	}
	public var isTimeBased: Bool {
		return self == .time || self == .timeRestUndefined
	}
	public var isDistanceBased: Bool {
		return self == .distance || self == .distanceRestUndefined
	}
	
	public var displayString: String {
		switch self {
		case .time: return "Time"
		case .distance: return "Distance"
		case .rest: return "Rest"
		case .timeRestUndefined: return "Time Rest Undefined"
		case .distanceRestUndefined: return "Distance Rest Undefined"
		case .restUndefined: return "Rest Undefined"
		case .calories: return "Calories"
		case .caloriesRestUndefined: return "Calories Rest Undefined"
		case .wattMinutes: return "Watt Minutes"
		case .wattMinutesRestUndefined: return "Watt Minutes Rest Undefined"
		case .none: return "None"
		}
	}
}
