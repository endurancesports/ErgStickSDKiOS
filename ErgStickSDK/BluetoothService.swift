//
//  BluetoothService.swift
//  ErgStick
//
//  Created by Nikola Milicevic on 12/8/17.
//  Copyright © 2017 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation
import CoreBluetooth

protocol RowingPeripheral {
	init(peripheral: CBPeripheral)
	
	var peripheral: CBPeripheral { get }
	var services: [CBUUID] { get }
	var characteristics: [CBUUID] { get }
	
	func peripheral(_ peripheral: CBPeripheral, didDiscover characteristic: CBCharacteristic)
	func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic)
}

protocol HeartRatePeripheral: RowingPeripheral {
	var bpm: Float { get }
}

enum ScanningMode {
	case rowingMonitor
	case heartRateMonitor
}

class BluetoothService: NSObject {
	
	// MARK: - Properties
	
	lazy var central = CBCentralManager(delegate: self, queue: nil)
	
	private var discoveredPeripherals: [CBPeripheral]? {
		didSet {
			let peripherals = discoveredPeripherals?.map { $0.name! }
			NotificationCenter.default.post(name: .didDiscoverPeripheral, object: peripherals)
		}
	}
	var rowingMonitor: RowingPeripheral?
	var heartRateMonitor: HeartRatePeripheral?
	
	var scanningMode: ScanningMode = .rowingMonitor
	
	// MARK: - Peripheral operations
	
	func scan(for mode: ScanningMode) {
		scanningMode = mode
		
		guard central.state == .poweredOn else { return }
		switch mode {
		case .rowingMonitor:
			central.scanForPeripherals(withServices: nil, options: nil)
		case .heartRateMonitor:
			central.scanForPeripherals(withServices: [CBUUID(string: "180D")], options: nil)
		}
	}
	
	func stopScan() {
		central.stopScan()
	}
	
	func cleanUp() {
		discoveredPeripherals = nil
	}
	
	func connect(at index: Int) {
		central.connect(discoveredPeripherals![index], options: nil)
	}
	
	func disconnect(from mode: ScanningMode) {
		switch mode {
		case .rowingMonitor:
			guard let _ = rowingMonitor else { return }
			central.cancelPeripheralConnection(rowingMonitor!.peripheral)
		case .heartRateMonitor:
			guard let _ = heartRateMonitor else { return }
			central.cancelPeripheralConnection(heartRateMonitor!.peripheral)
		}
	}
}

// MARK: - CBCentralManagerDelegate

extension BluetoothService: CBCentralManagerDelegate {
	func centralManagerDidUpdateState(_ central: CBCentralManager) {
		scan(for: scanningMode)
	}
	
	func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
		addToDiscovered(peripheral)
	}
	
	func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
		central.stopScan()
		peripheral.delegate = self
		
		if peripheral.isPM5 {
			rowingMonitor = PM5(peripheral: peripheral)
			peripheral.discoverServices(rowingMonitor?.services)
		} else if peripheral.isErgStick {
			rowingMonitor = ErgStick(peripheral: peripheral)
			peripheral.discoverServices(rowingMonitor?.services)
		} else {
			heartRateMonitor = HeartRateMonitor(peripheral: peripheral)
			peripheral.discoverServices(heartRateMonitor?.services)
		}
		
		print("connected to: \(peripheral.name!)")
		NotificationCenter.default.post(name: .peripheralConnectivityDidChange, object: peripheral.name!)
	}
	
	func centralManager(_ central: CBCentralManager, didFailToConnect peripheral: CBPeripheral, error: Error?) {
		// cleanup
		if peripheral.isPM5 || peripheral.isErgStick {
			rowingMonitor = nil
		} else {
			heartRateMonitor = nil
		}
		print("did fail to connecto to:", peripheral.name!)
		NotificationCenter.default.post(name: .peripheralConnectivityDidChange, object: nil)
	}
	
	func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
		if error != nil {
			print(#function, error!)
		}
		
		// cleanup
		if peripheral.isPM5 || peripheral.isErgStick {
			rowingMonitor = nil
		} else {
			heartRateMonitor = nil
		}
		
		print("disconnected from:", peripheral.name!)
		NotificationCenter.default.post(name: .peripheralConnectivityDidChange, object: nil)
	}
}

// MARK: - CBPeripheralDelegate

extension BluetoothService: CBPeripheralDelegate {
	func peripheral(_ peripheral: CBPeripheral, didDiscoverServices error: Error?) {
		guard let services = peripheral.services else { return }
		if peripheral.isPM5 || peripheral.isErgStick {
			for service in services {
				peripheral.discoverCharacteristics(rowingMonitor?.characteristics, for: service)
			}
		} else {
			for service in services {
				peripheral.discoverCharacteristics(heartRateMonitor?.characteristics, for: service)
			}
		}
	}
	
	func peripheral(_ peripheral: CBPeripheral, didDiscoverCharacteristicsFor service: CBService, error: Error?) {
		guard let characteristics = service.characteristics else { return }
		if peripheral.isPM5 || peripheral.isErgStick {
			for characteristic in characteristics {
				rowingMonitor?.peripheral(peripheral, didDiscover: characteristic)
			}
		} else {
			for characteristic in characteristics {
				heartRateMonitor?.peripheral(peripheral, didDiscover: characteristic)
			}
		}
	}
	
	func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic, error: Error?) {
		if peripheral.isPM5 || peripheral.isErgStick {
			rowingMonitor?.peripheral(peripheral, didUpdateValueFor: characteristic)
		} else {
			heartRateMonitor?.peripheral(peripheral, didUpdateValueFor: characteristic)
		}
	}
}

// MARK: - Helpers

private extension BluetoothService {
	func addToDiscovered(_ peripheral: CBPeripheral) {
		if scanningMode == .rowingMonitor {
			guard peripheral.isPM5 || peripheral.isErgStick else { return }
		} else {
			guard let _ = peripheral.name else { return }
		}
		
		if discoveredPeripherals == nil { discoveredPeripherals = [CBPeripheral]() }
		if discoveredPeripherals!.contains(peripheral) { return }
		
		discoveredPeripherals!.append(peripheral)
		print(discoveredPeripherals!)
		print("discovered: \(peripheral.name!)")
	}
}
