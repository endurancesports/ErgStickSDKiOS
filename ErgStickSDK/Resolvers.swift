//
//  Resolvers.swift
//  ErgStick
//
//  Created by Nikola Milicevic on 2/2/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

protocol IntervalResolver {
	func feed(_ buffer: RowingBuffer)
	func resolve(_ buffer: IntervalBuffer)
	func resolveIncomplete(_ buffer: IntervalBuffer)
}

protocol SessionResolver {
	func feed(_ buffer: RowingBuffer)
	func resolve(_ buffer: SessionBuffer)
}

class TimeIntervalResolver: IntervalResolver {
	private var splitIntervalTime: Float = 0.0
	private var splitIntervalDistance: Float = 0.0
	private var intervalRestTime: Float = 0.0
	private var intervalRestDistance: Float = 0.0
	private var splitIntervalType: IntervalType = .none
	private var splitIntervalNumber: Int = 0
	private var splitIntervalAverageStrokeRate: Float = 0.0
	private var splitIntervalAveragePace: Float = 0.0
	private var splitIntervalTotalCalories: Float = 0.0
	private var splitIntervalAverageCalories: Float = 0.0
	private var splitIntervalSpeed: Float = 0.0
	private var splitIntervalPower: Float = 0.0
	private var splitIntervalAverageDragFactor: Float = 0.0
	
	// incomplete data
	private var elapsedTime: Float = 0.0
	private var elapsedDistance: Float = 0.0
	private var incompleteStrokeRate = PositiveAverage()
	private var incompleteCalories = PositiveAverage()
	private var incompleteSpeed = PositiveAverage()
	private var incompletePower = PositiveAverage()
	private var incompleteDragFactor = PositiveAverage()
	
	func feed(_ buffer: RowingBuffer) {
		intervalRestDistance = max(intervalRestDistance, buffer.restDistance)
		intervalRestTime = max(intervalRestTime, buffer.restTime)
		splitIntervalDistance = buffer.lastSplitDistance
		splitIntervalTime = buffer.workoutDuration
		splitIntervalPower = buffer.splitAvgPower > 0 ? buffer.splitAvgPower : splitIntervalPower
		splitIntervalAverageStrokeRate = buffer.splitIntervalAverageStrokeRate > 0 ? buffer.splitIntervalAverageStrokeRate : splitIntervalAverageStrokeRate
		
		splitIntervalAveragePace = 0
		if buffer.lastSplitDistance > 0 {
			splitIntervalAveragePace = splitIntervalTime.magicPM5RoundingUp * 500 / buffer.lastSplitDistance
		}
		
		if buffer.intervalType.isWorkInterval {
			splitIntervalType = buffer.intervalType
		}
		splitIntervalNumber = buffer.intervalCount
		splitIntervalTotalCalories = buffer.splitIntervalTotalCalories
		splitIntervalAverageCalories = buffer.splitIntervalAverageCalories
		splitIntervalSpeed = buffer.splitIntervalSpeed
		splitIntervalAverageDragFactor = buffer.splitIntervalAverageDragFactor
		
		// incomplete data
		elapsedTime = buffer.elapsedTime
		elapsedDistance = buffer.distance
		incompleteStrokeRate.add(value: Float(buffer.strokeRate))
		incompleteCalories.add(value: buffer.strokeCalories)
		incompleteSpeed.add(value: buffer.speed)
		incompletePower.add(value: buffer.strokePower)
		incompleteDragFactor.add(value: buffer.dragFactor)
	}
	
	func resolve(_ buffer: IntervalBuffer) {
		buffer.elapsedTime = splitIntervalTime
		buffer.elapsedDistance = splitIntervalDistance
		buffer.restTime = intervalRestTime
		buffer.restDistance = intervalRestDistance
		buffer.intervalType = splitIntervalType
		buffer.avgStrokeRate = splitIntervalAverageStrokeRate
		buffer.avgPace = splitIntervalAveragePace
		buffer.totalCalories = splitIntervalTotalCalories
		buffer.avgCalories = splitIntervalAverageCalories
		buffer.avgSpeed = splitIntervalSpeed
		buffer.avgPower = splitIntervalPower
		buffer.avgDragFactor = splitIntervalAverageDragFactor
		reset()
	}
	
	func resolveIncomplete(_ buffer: IntervalBuffer) {
		buffer.elapsedTime = elapsedTime
		buffer.elapsedDistance = elapsedDistance
		
		buffer.restTime = intervalRestTime
		buffer.restDistance = intervalRestDistance
		buffer.intervalType = splitIntervalType
		
		buffer.avgStrokeRate = incompleteStrokeRate.getAverage()
		buffer.avgPace = elapsedTime.magicPM5RoundingUp * 500 / floorf(elapsedDistance)
		buffer.totalCalories = incompleteCalories.getTotal()
		buffer.avgCalories = incompleteCalories.getAverage()
		buffer.avgSpeed = incompleteSpeed.getAverage()
		buffer.avgPower = incompletePower.getAverage()
		buffer.avgDragFactor = incompleteDragFactor.getAverage()
		
		reset()
	}

	private func reset() {
		splitIntervalTime = 0
		splitIntervalDistance = 0
		intervalRestTime = 0
		intervalRestDistance = 0
		splitIntervalType = .none
		splitIntervalNumber = 0
		splitIntervalAverageStrokeRate = 0
		splitIntervalAveragePace = 0
		splitIntervalTotalCalories = 0
		splitIntervalAverageCalories = 0
		splitIntervalSpeed = 0
		splitIntervalPower = 0
		splitIntervalAverageDragFactor = 0
		
		// incomplete data
		elapsedTime = 0
		elapsedDistance = 0
		incompleteStrokeRate = PositiveAverage()
		incompleteCalories = PositiveAverage()
		incompleteSpeed = PositiveAverage()
		incompletePower = PositiveAverage()
		incompleteDragFactor = PositiveAverage()
	}
}

class TimeIntervalSessionResolver: SessionResolver {
	private var totalWorkTime: Float = 0.0
	private var totalWorkDistance: Float = 0.0
	private var splitIntervalNumber: Int = 0
	private var intervalRestDistance: Float = 0.0
	private var totalRestDistance: Float = 0.0
	
	// incomplete data
	private var splitIntervalDistance: Float = 0.0
	private var splitIntervalTime: Float = 0.0
	
	func feed(_ buffer: RowingBuffer) {
		intervalRestDistance = max(intervalRestDistance, buffer.restDistance)
		
		if splitIntervalNumber + 1 == buffer.intervalCount && !buffer.intervalType.isRestInterval {
			splitIntervalNumber = buffer.intervalCount
			totalWorkTime = floor(buffer.workoutDuration)
			totalRestDistance += intervalRestDistance
			intervalRestDistance = 0
		}
		
		totalWorkDistance = buffer.totalWorkDistance - totalRestDistance
		
		// incomplete data
		if buffer.splitIntervalDistance != 0 {
			splitIntervalDistance = buffer.splitIntervalDistance
		} else if buffer.lastSplitDistance != 0 {
			splitIntervalDistance = buffer.lastSplitDistance
		} else if buffer.splitIntervalDistance != 0 {
			splitIntervalDistance = buffer.splitIntervalDistance
		} else {
			splitIntervalDistance = floorf(buffer.distance)
		}
		splitIntervalTime = buffer.elapsedTime
	}
	
	func resolve(_ buffer: SessionBuffer) {
		// incomplete data
		let distance = totalWorkDistance != 0 ? totalWorkDistance : splitIntervalDistance
		let time = totalWorkTime != 0 ? totalWorkTime : splitIntervalTime
		buffer.distance = distance
		buffer.elapsedTime = time
		
		reset()
	}
	
	private func reset() {
		totalWorkDistance = 0
		totalWorkTime = 0
		splitIntervalNumber = 0
		totalRestDistance = 0
		intervalRestDistance = 0
		
		// incomplete data
		splitIntervalDistance = 0
		splitIntervalTime = 0
	}
}

class VariableIntervalsResolver: IntervalResolver {
	private var splitIntervalTime: Float = 0.0
	private var splitIntervalDistance: Float = 0.0
	private var intervalRestTime: Float = 0.0
	private var intervalRestDistance: Float = 0.0
	private var splitIntervalType: IntervalType = .none
	private var splitIntervalNumber: Int = 0
	private var splitIntervalAverageStrokeRate: Float = 0.0
	private var splitIntervalAveragePace: Float = 0.0
	private var splitIntervalTotalCalories: Float = 0.0
	private var splitIntervalAverageCalories: Float = 0.0
	private var splitIntervalSpeed: Float = 0.0
	private var splitIntervalPower: Float = 0.0
	private var splitIntervalAverageDragFactor: Float = 0.0
	
	private var finishedIntervals: Int = 0
	private var currentWorkInterval: IntervalType = .none
	
	// incomplete data
	private var elapsedTime: Float = 0.0
	private var elapsedDistance: Float = 0.0
	private var incompleteStrokeRate = PositiveAverage()
	private var incompleteCalories = PositiveAverage()
	private var incompleteSpeed = PositiveAverage()
	private var incompletePower = PositiveAverage()
	private var incompleteDragFactor = PositiveAverage()

	func feed(_ buffer: RowingBuffer) {
		if buffer.intervalType.isWorkInterval && buffer.intervalCount == finishedIntervals {
			currentWorkInterval = buffer.intervalType
		}
		
		// distance
		if currentWorkInterval.isDistanceBased {
			if buffer.intervalCount == finishedIntervals {
				splitIntervalDistance = buffer.workoutDistance
			}
			if buffer.intervalCount > finishedIntervals {
				splitIntervalTime = 0
			}
			splitIntervalTime = max(buffer.lastSplitTime, splitIntervalTime)
			
			splitIntervalAveragePace = 0
			if splitIntervalDistance > 0 {
				splitIntervalAveragePace = splitIntervalTime * 500 / splitIntervalDistance
			}
		}
		
		// time
		if currentWorkInterval.isTimeBased {
			splitIntervalDistance = buffer.lastSplitDistance
			if buffer.intervalCount == finishedIntervals {
				splitIntervalTime = buffer.workoutDuration
			}
			splitIntervalAveragePace = 0
			if buffer.lastSplitDistance > 0 {
				splitIntervalAveragePace = splitIntervalTime.magicPM5RoundingUp * 500 / buffer.lastSplitDistance
			}
		}
		
		intervalRestDistance = max(intervalRestDistance, buffer.restDistance)
		intervalRestTime = max(intervalRestTime, buffer.restTime)
		splitIntervalPower = buffer.splitAvgPower > 0 ? buffer.splitAvgPower : splitIntervalPower
		splitIntervalAverageStrokeRate = buffer.splitIntervalAverageStrokeRate > 0 ? buffer.splitIntervalAverageStrokeRate : splitIntervalAverageStrokeRate
		
		if buffer.intervalType.isWorkInterval {
			splitIntervalType = buffer.intervalType
		}
		splitIntervalNumber = buffer.intervalCount
		splitIntervalTotalCalories = buffer.splitIntervalTotalCalories
		splitIntervalAverageCalories = buffer.splitIntervalAverageCalories
		splitIntervalSpeed = buffer.splitIntervalSpeed
		splitIntervalAverageDragFactor = buffer.splitIntervalAverageDragFactor
		
		// incomplete data
		elapsedTime = buffer.elapsedTime
		elapsedDistance = buffer.distance
		incompleteStrokeRate.add(value: Float(buffer.strokeRate))
		incompleteCalories.add(value: buffer.strokeCalories)
		incompleteSpeed.add(value: buffer.speed)
		incompletePower.add(value: buffer.strokePower)
		incompleteDragFactor.add(value: buffer.dragFactor)
	}
	
	func resolve(_ buffer: IntervalBuffer) {
		buffer.elapsedTime = splitIntervalTime
		buffer.elapsedDistance = splitIntervalDistance
		buffer.restTime = intervalRestTime
		buffer.restDistance = intervalRestDistance
		buffer.intervalType = splitIntervalType
		buffer.avgStrokeRate = splitIntervalAverageStrokeRate
		buffer.avgPace = splitIntervalAveragePace
		buffer.totalCalories = splitIntervalTotalCalories
		buffer.avgCalories = splitIntervalAverageCalories
		buffer.avgSpeed = splitIntervalSpeed
		buffer.avgPower = splitIntervalPower
		buffer.avgDragFactor = splitIntervalAverageDragFactor
		reset()
		finishedIntervals += 1
	}
	
	func resolveIncomplete(_ buffer: IntervalBuffer) {
		buffer.elapsedTime = elapsedTime
		buffer.elapsedDistance = elapsedDistance
		
		buffer.restTime = intervalRestTime
		buffer.restDistance = intervalRestDistance
		buffer.intervalType = splitIntervalType
		
		buffer.avgStrokeRate = incompleteStrokeRate.getAverage()
		buffer.avgPace = elapsedTime.magicPM5RoundingUp * 500 / floorf(elapsedDistance)
		buffer.totalCalories = incompleteCalories.getTotal()
		buffer.avgCalories = incompleteCalories.getAverage()
		buffer.avgSpeed = incompleteSpeed.getAverage()
		buffer.avgPower = incompletePower.getAverage()
		buffer.avgDragFactor = incompleteDragFactor.getAverage()
		
		reset()
	}

	private func reset() {
		splitIntervalTime = 0
		splitIntervalDistance = 0
		intervalRestTime = 0
		intervalRestDistance = 0
		splitIntervalType = .none
		splitIntervalNumber = 0
		splitIntervalAverageStrokeRate = 0
		splitIntervalAveragePace = 0
		splitIntervalTotalCalories = 0
		splitIntervalAverageCalories = 0
		splitIntervalSpeed = 0
		splitIntervalPower = 0
		splitIntervalAverageDragFactor = 0
		
		// incomplete data
		elapsedTime = 0
		elapsedDistance = 0
		incompleteStrokeRate = PositiveAverage()
		incompleteCalories = PositiveAverage()
		incompleteSpeed = PositiveAverage()
		incompletePower = PositiveAverage()
		incompleteDragFactor = PositiveAverage()
	}
}

class VariableIntervalsSessionResolver: SessionResolver {
	private var totalTime: Float = 0.0
	private var totalDistance: Float = 0.0

	private var splitIntervalTime: Float = 0.0
	private var splitIntervalDistance: Float = 0.0
	private var intervalRestDistance: Float = 0.0
	
	private var finishedIntervals: UInt32 = 0
	private var currentWorkInterval: IntervalType = .none
	private var totalRestDistance: Float = 0.0
	
	// incomplete data
	private var currentDistance: Float = 0.0
	private var currentTime: Float = 0.0
	
	func feed(_ buffer: RowingBuffer) {
		if buffer.workoutState.rawValue > 0 && Int(currentWorkInterval.rawValue) < 0 {
			currentWorkInterval = buffer.intervalType
		}
		
		// distance
		if currentWorkInterval.isDistanceBased {
			splitIntervalTime = max(buffer.lastSplitTime, splitIntervalTime)
		}
		
		// time
		if currentWorkInterval.isTimeBased {
			splitIntervalTime = buffer.workoutDuration
		}
		
		totalDistance = buffer.totalWorkDistance
		intervalRestDistance = max(intervalRestDistance, buffer.restDistance)
		
		if (buffer.workoutState.rawValue > 0 && buffer.intervalType.isWorkInterval && buffer.intervalCount == finishedIntervals + 1) ||
			(buffer.workoutState.rawValue >= 12 && buffer.intervalType.isRestInterval) {
			
			totalTime += splitIntervalTime
			totalRestDistance += intervalRestDistance
			reset()
			currentWorkInterval = buffer.intervalType
			finishedIntervals += 1
		}
		
		totalDistance = buffer.totalWorkDistance - totalRestDistance
		
		// incomplete data
		if buffer.splitIntervalDistance != 0 {
			currentDistance = buffer.splitIntervalDistance
		} else if buffer.lastSplitDistance != 0 {
			currentDistance = buffer.lastSplitDistance
		} else if buffer.splitIntervalDistance != 0 {
			currentDistance = buffer.splitIntervalDistance
		} else {
			currentDistance = floorf(buffer.distance)
		}
		currentTime = buffer.elapsedTime
	}
	
	func resolve(_ buffer: SessionBuffer) {
		// incomplete data
		let distance = totalDistance != 0 ? totalDistance : currentDistance
		let time = totalTime != 0 ? totalTime : currentTime
		buffer.distance = distance
		buffer.elapsedTime = time
	}
	
	private func reset() {
		splitIntervalTime = 0
		splitIntervalDistance = 0
		intervalRestDistance = 0
	}
}

class SingleTimeResolver: IntervalResolver {
	private var splitIntervalTime: Float = 0.0
	private var splitIntervalDistance: Float = 0.0
	private var intervalRestTime: Float = 0.0
	private var intervalRestDistance: Float = 0.0
	private var splitIntervalType: IntervalType = .none
	private var splitIntervalNumber: Int = 0
	private var splitIntervalAverageStrokeRate: Float = 0.0
	private var splitIntervalAveragePace: Float = 0.0
	private var splitIntervalTotalCalories: Float = 0.0
	private var splitIntervalAverageCalories: Float = 0.0
	private var splitIntervalSpeed: Float = 0.0
	private var splitIntervalPower: Float = 0.0
	private var splitIntervalAverageDragFactor: Float = 0.0
	
	private var splitLength: Float = 0.0
	
	// incomplete data
	private var elapsedTime: Float = 0.0
	private var elapsedDistance: Float = 0.0
	private var incompleteStrokeRate = PositiveAverage()
	private var incompleteCalories = PositiveAverage()
	private var incompleteSpeed = PositiveAverage()
	private var incompletePower = PositiveAverage()
	private var incompleteDragFactor = PositiveAverage()
	
	private var splitTimes = [Float]()
	private var splitDistances = [Float]()
	
	func feed(_ buffer: RowingBuffer) {
		// determine split length
		if buffer.intervalCount == 0 {
			splitLength = buffer.elapsedTime.roundToNearest10
		}
		
		intervalRestDistance = max(intervalRestDistance, buffer.restDistance)
		intervalRestTime = max(intervalRestTime, buffer.restTime)
		splitIntervalDistance = buffer.lastSplitDistance
		splitIntervalPower = buffer.splitAvgPower > 0 ? buffer.splitAvgPower : splitIntervalPower
		splitIntervalAverageStrokeRate = buffer.splitIntervalAverageStrokeRate > 0 ? buffer.splitIntervalAverageStrokeRate : splitIntervalAverageStrokeRate
		
		splitIntervalTime = splitLength
		splitIntervalAveragePace = 0
		
		if buffer.lastSplitDistance > 0 {
			splitIntervalAveragePace = splitIntervalTime.magicPM5RoundingUp * 500 / buffer.lastSplitDistance
		}
		
		if buffer.intervalType.isWorkInterval {
			splitIntervalType = buffer.intervalType
		}
		
		splitIntervalTotalCalories = buffer.splitIntervalTotalCalories
		splitIntervalAverageCalories = buffer.splitIntervalAverageCalories
		splitIntervalSpeed = buffer.splitIntervalSpeed
		splitIntervalAverageDragFactor = buffer.splitIntervalAverageDragFactor
		
		// incomplete data
		elapsedTime = buffer.elapsedTime
		elapsedDistance = buffer.distance
		incompleteStrokeRate.add(value: Float(buffer.strokeRate))
		incompleteCalories.add(value: buffer.strokeCalories)
		incompleteSpeed.add(value: buffer.speed)
		incompletePower.add(value: buffer.strokePower)
		incompleteDragFactor.add(value: buffer.dragFactor)
	}
	
	func resolve(_ buffer: IntervalBuffer) {
		buffer.elapsedTime = splitIntervalTime
		buffer.elapsedDistance = splitIntervalDistance
		
		// incomplete data
		splitTimes.append(splitIntervalTime)
		splitDistances.append(splitIntervalDistance)
		
		buffer.restTime = intervalRestTime
		buffer.restDistance = intervalRestDistance
		buffer.intervalType = splitIntervalType
		buffer.avgStrokeRate = splitIntervalAverageStrokeRate
		buffer.avgPace = splitIntervalAveragePace
		buffer.totalCalories = splitIntervalTotalCalories
		buffer.avgCalories = splitIntervalAverageCalories
		buffer.avgSpeed = splitIntervalSpeed
		buffer.avgPower = splitIntervalPower
		buffer.avgDragFactor = splitIntervalAverageDragFactor
		reset()
	}
	
	func resolveIncomplete(_ buffer: IntervalBuffer) {
		let pastTime = splitTimes.reduce(0, +)
		let time = elapsedTime - pastTime
		buffer.elapsedTime = time
		
		let pastDistance = splitDistances.reduce(0, +)
		let distance = floorf(elapsedDistance - pastDistance)
		buffer.elapsedDistance = distance
		
		buffer.restTime = intervalRestTime
		buffer.restDistance = intervalRestDistance
		buffer.intervalType = splitIntervalType
		
		buffer.avgStrokeRate = incompleteStrokeRate.getAverage()
		buffer.avgPace = time.magicPM5RoundingUp * 500 / distance
		buffer.totalCalories = incompleteCalories.getTotal()
		buffer.avgCalories = incompleteCalories.getAverage()
		buffer.avgSpeed = incompleteSpeed.getAverage()
		buffer.avgPower = incompletePower.getAverage()
		buffer.avgDragFactor = incompleteDragFactor.getAverage()
		
		reset()
	}

	private func reset() {
		splitIntervalDistance = 0
		intervalRestTime = 0
		intervalRestDistance = 0
		splitIntervalType = .none
		splitIntervalNumber = 0
		splitIntervalAverageStrokeRate = 0
		splitIntervalAveragePace = 0
		splitIntervalTotalCalories = 0
		splitIntervalAverageCalories = 0
		splitIntervalSpeed = 0
		splitIntervalPower = 0
		splitIntervalAverageDragFactor = 0
		
		// incomplete data
		elapsedTime = 0
		elapsedDistance = 0
		incompleteStrokeRate = PositiveAverage()
		incompleteCalories = PositiveAverage()
		incompleteSpeed = PositiveAverage()
		incompletePower = PositiveAverage()
		incompleteDragFactor = PositiveAverage()
	}
}

class SingleTimeSessionResolver: SessionResolver {
	private var totalWorkTime: Float = 0.0
	private var totalWorkDistance: Float = 0.0
	
	func feed(_ buffer: RowingBuffer) {
		totalWorkDistance = buffer.distance
		totalWorkTime = buffer.elapsedTime
	}
	
	func resolve(_ buffer: SessionBuffer) {
		buffer.distance = totalWorkDistance
		buffer.elapsedTime = totalWorkTime
		
		reset()
	}
	
	private func reset() {
		totalWorkDistance = 0
		totalWorkTime = 0
	}
}

class DistanceIntervalResolver: IntervalResolver {
	private var splitIntervalTime: Float = 0.0
	private var splitIntervalDistance: Float = 0.0
	private var intervalRestTime: Float = 0.0
	private var intervalRestDistance: Float = 0.0
	private var splitIntervalType: IntervalType = .none
	private var splitIntervalNumber: Int = 0
	private var splitIntervalAverageStrokeRate: Float = 0.0
	private var splitIntervalAveragePace: Float = 0.0
	private var splitIntervalTotalCalories: Float = 0.0
	private var splitIntervalAverageCalories: Float = 0.0
	private var splitIntervalSpeed: Float = 0.0
	private var splitIntervalPower: Float = 0.0
	private var splitIntervalAverageDragFactor: Float = 0.0
	
	private var finishedIntervals = 0
	
	// incomplete data
	private var elapsedTime: Float = 0.0
	private var elapsedDistance: Float = 0.0
	private var incompleteStrokeRate = PositiveAverage()
	private var incompleteCalories = PositiveAverage()
	private var incompleteSpeed = PositiveAverage()
	private var incompletePower = PositiveAverage()
	private var incompleteDragFactor = PositiveAverage()
	
	func feed(_ buffer: RowingBuffer) {
		intervalRestDistance = max(intervalRestDistance, buffer.restDistance)
		intervalRestTime = max(intervalRestTime, buffer.restTime)
		splitIntervalDistance = buffer.workoutDistance
		splitIntervalTime = buffer.lastSplitTime
		splitIntervalPower = buffer.splitAvgPower > 0 ? buffer.splitAvgPower : splitIntervalPower
		splitIntervalAverageStrokeRate = buffer.splitIntervalAverageStrokeRate > 0 ? buffer.splitIntervalAverageStrokeRate : splitIntervalAverageStrokeRate
		
		splitIntervalAveragePace = 0
		if splitIntervalDistance > 0 {
			splitIntervalAveragePace = splitIntervalTime.magicPM5RoundingUp * 500 / splitIntervalDistance
		}
		
		if buffer.intervalType.isWorkInterval {
			splitIntervalType = buffer.intervalType
		}

		splitIntervalNumber = buffer.intervalCount
		splitIntervalTotalCalories = buffer.splitIntervalTotalCalories
		splitIntervalAverageCalories = buffer.splitIntervalAverageCalories
		splitIntervalSpeed = buffer.splitIntervalSpeed
		splitIntervalAverageDragFactor = buffer.splitIntervalAverageDragFactor
		
		// incomplete data
		elapsedTime = buffer.elapsedTime
		elapsedDistance = buffer.distance
		incompleteStrokeRate.add(value: Float(buffer.strokeRate))
		incompleteCalories.add(value: buffer.strokeCalories)
		incompleteSpeed.add(value: buffer.speed)
		incompletePower.add(value: buffer.strokePower)
		incompleteDragFactor.add(value: buffer.dragFactor)
	}
	
	func resolve(_ buffer: IntervalBuffer) {
		buffer.elapsedTime = splitIntervalTime
		buffer.elapsedDistance = splitIntervalDistance
		buffer.restTime = intervalRestTime
		buffer.restDistance = intervalRestDistance
		buffer.intervalType = splitIntervalType
		buffer.avgStrokeRate = splitIntervalAverageStrokeRate
		buffer.avgPace = splitIntervalAveragePace
		buffer.totalCalories = splitIntervalTotalCalories
		buffer.avgCalories = splitIntervalAverageCalories
		buffer.avgSpeed = splitIntervalSpeed
		buffer.avgPower = splitIntervalPower
		buffer.avgDragFactor = splitIntervalAverageDragFactor
		finishedIntervals += 1
		reset()
	}
	
	func resolveIncomplete(_ buffer: IntervalBuffer) {
		buffer.elapsedTime = elapsedTime
		
		// PM3 bug
		/*
		if let plannedDistanceOrTime = buffer.plannedDistanceOrTime {
			buffer.elapsedDistance = plannedDistanceOrTime - elapsedDistance
		}
		*/
		
		buffer.elapsedDistance = elapsedDistance
		
		buffer.restTime = intervalRestTime
		buffer.restDistance = intervalRestDistance
		buffer.intervalType = splitIntervalType
		
		buffer.avgStrokeRate = incompleteStrokeRate.getAverage()
		buffer.avgPace = elapsedTime.magicPM5RoundingUp * 500 / floorf(elapsedDistance)
		buffer.totalCalories = incompleteCalories.getTotal()
		buffer.avgCalories = incompleteCalories.getAverage()
		buffer.avgSpeed = incompleteSpeed.getAverage()
		buffer.avgPower = incompletePower.getAverage()
		buffer.avgDragFactor = incompleteDragFactor.getAverage()
		
		reset()
	}

	private func reset() {
		splitIntervalTime = 0
		splitIntervalDistance = 0
		intervalRestTime = 0
		intervalRestDistance = 0
		splitIntervalType = .none
		splitIntervalNumber = 0
		splitIntervalAverageStrokeRate = 0
		splitIntervalAveragePace = 0
		splitIntervalTotalCalories = 0
		splitIntervalAverageCalories = 0
		splitIntervalSpeed = 0
		splitIntervalPower = 0
		splitIntervalAverageDragFactor = 0
		
		// incomplete data
		elapsedTime = 0
		elapsedDistance = 0
		incompleteStrokeRate = PositiveAverage()
		incompleteCalories = PositiveAverage()
		incompleteSpeed = PositiveAverage()
		incompletePower = PositiveAverage()
		incompleteDragFactor = PositiveAverage()
	}
}

class DistanceIntervalsSessionResolver: SessionResolver {
	private var totalTime: Float = 0.0
	private var totalDistance: Float = 0.0
	private var splitIntervalTime: Float = 0.0
	private var splitIntervalDistance: Float = 0.0
	private var finishedIntervals = 0
	
	func feed(_ buffer: RowingBuffer) {
		splitIntervalTime = buffer.lastSplitTime
		splitIntervalDistance = max(buffer.workoutDistance, splitIntervalDistance)
		
		if (buffer.workoutState.rawValue > 0 && buffer.intervalType.isWorkInterval && buffer.intervalCount == finishedIntervals + 1) ||
			(buffer.workoutState.rawValue >= 12 && buffer.intervalType.isRestInterval) {
			totalTime += splitIntervalTime
			totalDistance += splitIntervalDistance
			reset()
			finishedIntervals += 1
		}
		
		totalDistance = buffer.totalWorkDistance - buffer.restDistance
	}
	
	func resolve(_ buffer: SessionBuffer) {
		buffer.elapsedTime = totalTime
		buffer.distance = totalDistance
	}
	
	private func reset() {
		splitIntervalTime = 0
		splitIntervalDistance = 0
	}
}

class VariableIntervalsRestUndefinedResolver: IntervalResolver {
	private var splitIntervalTime: Float = 0.0
	private var splitIntervalDistance: Float = 0.0
	private var intervalRestTime: Float = 0.0
	private var intervalRestDistance: Float = 0.0
	private var splitIntervalType: IntervalType = .none
	private var splitIntervalNumber: Int = 0
	private var splitIntervalAverageStrokeRate: Float = 0.0
	private var splitIntervalAveragePace: Float = 0.0
	private var splitIntervalTotalCalories: Float = 0.0
	private var splitIntervalAverageCalories: Float = 0.0
	private var splitIntervalSpeed: Float = 0.0
	private var splitIntervalPower: Float = 0.0
	private var splitIntervalAverageDragFactor: Float = 0.0
	
	private var finishedIntervals: Int = 0
	private var currentWorkInterval: IntervalType = .none
	
	// incomplete data
	private var elapsedTime: Float = 0.0
	private var elapsedDistance: Float = 0.0
	private var incompleteStrokeRate = PositiveAverage()
	private var incompleteCalories = PositiveAverage()
	private var incompleteSpeed = PositiveAverage()
	private var incompletePower = PositiveAverage()
	private var incompleteDragFactor = PositiveAverage()
	
	func feed(_ buffer: RowingBuffer) {
        //print("interval resolver: \(buffer.intervalType.displayString), buffer.restTime: \(buffer.restTime), buffer.restDistance: \(buffer.restDistance), splitIntervalTime: \(splitIntervalTime), splitIntervalDistance: \(splitIntervalDistance) ")
		if buffer.intervalType.isWorkInterval && buffer.intervalCount == finishedIntervals {
			currentWorkInterval = buffer.intervalType
		}
		
		// distance
		if currentWorkInterval.isDistanceBased {
			if buffer.intervalCount == finishedIntervals {
				splitIntervalDistance = buffer.workoutDistance
			}
			
			if buffer.intervalCount > finishedIntervals {
				splitIntervalTime = 0
			}
			splitIntervalTime = max(buffer.lastSplitTime, splitIntervalTime)
			
			splitIntervalAveragePace = 0
			if splitIntervalDistance > 0 {
				splitIntervalAveragePace = splitIntervalTime.magicPM5RoundingUp * 500 / splitIntervalDistance
			}
		}
		
		// time
		if currentWorkInterval.isTimeBased {
			splitIntervalDistance = buffer.lastSplitDistance
			if buffer.intervalCount == finishedIntervals {
				splitIntervalTime = buffer.workoutDuration
			}
			splitIntervalAveragePace = 0;
			if buffer.lastSplitDistance > 0 {
				splitIntervalAveragePace = buffer.workoutDuration * 500 / buffer.lastSplitDistance
			}
		}
		
		intervalRestDistance = max(intervalRestDistance, buffer.restDistance)
		intervalRestTime = max(intervalRestTime, buffer.restTime)
		splitIntervalPower = buffer.splitAvgPower > 0 ? buffer.splitAvgPower : splitIntervalPower
		splitIntervalAverageStrokeRate = buffer.splitIntervalAverageStrokeRate > 0 ? buffer.splitIntervalAverageStrokeRate : splitIntervalAverageStrokeRate

		if buffer.intervalType.isWorkInterval {
			splitIntervalType = buffer.intervalType
		}

		splitIntervalNumber = buffer.intervalCount
		splitIntervalTotalCalories = buffer.splitIntervalTotalCalories
		splitIntervalAverageCalories = buffer.splitIntervalAverageCalories
		splitIntervalSpeed = buffer.splitIntervalSpeed
		splitIntervalAverageDragFactor = buffer.splitIntervalAverageDragFactor
		
		// incomplete data
		elapsedTime = buffer.elapsedTime
		elapsedDistance = buffer.distance
		incompleteStrokeRate.add(value: Float(buffer.strokeRate))
		incompleteCalories.add(value: buffer.strokeCalories)
		incompleteSpeed.add(value: buffer.speed)
		incompletePower.add(value: buffer.strokePower)
		incompleteDragFactor.add(value: buffer.dragFactor)
	}
	
	func resolve(_ buffer: IntervalBuffer) {
        //print("interval resolver resolve: intervalRestTime: \(intervalRestTime), intervalRestDistance: \(intervalRestDistance)")
		buffer.elapsedTime = splitIntervalTime
		buffer.elapsedDistance = splitIntervalDistance
		buffer.restTime = intervalRestTime
		buffer.restDistance = intervalRestDistance
		buffer.intervalType = splitIntervalType
		buffer.avgStrokeRate = splitIntervalAverageStrokeRate
		buffer.avgPace = splitIntervalAveragePace
		buffer.totalCalories = splitIntervalTotalCalories
		buffer.avgCalories = splitIntervalAverageCalories
		buffer.avgSpeed = splitIntervalSpeed
		buffer.avgPower = splitIntervalPower
		buffer.avgDragFactor = splitIntervalAverageDragFactor
		reset()
		finishedIntervals += 1
	}
	
	func resolveIncomplete(_ buffer: IntervalBuffer) {
        //print("interval resolver resolveIncomplete: intervalRestTime: \(intervalRestTime), intervalRestDistance: \(intervalRestDistance)")
		buffer.elapsedTime = elapsedTime
		buffer.elapsedDistance = elapsedDistance
		
		buffer.restTime = intervalRestTime
		buffer.restDistance = intervalRestDistance
		buffer.intervalType = splitIntervalType
		
		buffer.avgStrokeRate = incompleteStrokeRate.getAverage()
		buffer.avgPace = elapsedTime.magicPM5RoundingUp * 500 / floorf(elapsedDistance)
		buffer.totalCalories = incompleteCalories.getTotal()
		buffer.avgCalories = incompleteCalories.getAverage()
		buffer.avgSpeed = incompleteSpeed.getAverage()
		buffer.avgPower = incompletePower.getAverage()
		buffer.avgDragFactor = incompleteDragFactor.getAverage()
		
		reset()
	}

	private func reset() {
		splitIntervalTime = 0
		splitIntervalDistance = 0
		intervalRestTime = 0
		intervalRestDistance = 0
		splitIntervalType = .none
		splitIntervalNumber = 0
		splitIntervalAverageStrokeRate = 0
		splitIntervalAveragePace = 0
		splitIntervalTotalCalories = 0
		splitIntervalAverageCalories = 0
		splitIntervalSpeed = 0
		splitIntervalPower = 0
		splitIntervalAverageDragFactor = 0
		
		// incomplete data
		elapsedTime = 0
		elapsedDistance = 0
		incompleteStrokeRate = PositiveAverage()
		incompleteCalories = PositiveAverage()
		incompleteSpeed = PositiveAverage()
		incompletePower = PositiveAverage()
		incompleteDragFactor = PositiveAverage()
	}
}

class VariableIntervalsRestUndefinedSessionResolver: SessionResolver {
	private var totalTime: Float = 0.0
	private var totalDistance: Float = 0.0
	
	private var splitIntervalTime: Float = 0.0
	
	private var finishedIntervals = 0
	private var currentWorkInterval: IntervalType = .none
	
	func feed(_ buffer: RowingBuffer) {
		if buffer.workoutState.rawValue > 0 && currentWorkInterval.rawValue < 0 {
			currentWorkInterval = buffer.intervalType
		}
		
		// distance
		if currentWorkInterval.isDistanceBased {
			splitIntervalTime = max(buffer.lastSplitTime, splitIntervalTime)
		}
		
		// time
		if currentWorkInterval.isTimeBased {
			splitIntervalTime = buffer.workoutDuration
		}
		
		totalDistance = buffer.totalWorkDistance - buffer.restDistance
		
		if (buffer.workoutState.rawValue > 0 && buffer.intervalType.isWorkInterval && buffer.intervalCount == finishedIntervals + 1) ||
			(buffer.workoutState.rawValue >= 12 && buffer.intervalType.isRestInterval) ||
			(buffer.workoutState.rawValue >= 10 && buffer.intervalType.isWorkInterval) {
			
			totalTime += splitIntervalTime
			reset()
			currentWorkInterval = buffer.intervalType
			finishedIntervals += 1
		}
	}
	
	func resolve(_ buffer: SessionBuffer) {
		buffer.elapsedTime = totalTime
		buffer.distance = totalDistance
	}
	
	private func reset() {
		splitIntervalTime = 0
	}
}

class SingleDistanceResolver: IntervalResolver {
	private var splitIntervalTime: Float = 0.0
	private var splitIntervalDistance: Float = 0.0
	private var intervalRestTime: Float = 0.0
	private var intervalRestDistance: Float = 0.0
	private var splitIntervalType: IntervalType = .none
	private var splitIntervalNumber: Int = 0
	private var splitIntervalAverageStrokeRate: Float = 0.0
	private var splitIntervalAveragePace: Float = 0.0
	private var splitIntervalTotalCalories: Float = 0.0
	private var splitIntervalAverageCalories: Float = 0.0
	private var splitIntervalSpeed: Float = 0.0
	private var splitIntervalPower: Float = 0.0
	private var splitIntervalAverageDragFactor: Float = 0.0
	
	private var splitLength: Float = 0.0
	
	// incomplete data
	private var elapsedTime: Float = 0.0
	private var elapsedDistance: Float = 0.0
	private var incompleteStrokeRate = PositiveAverage()
	private var incompleteCalories = PositiveAverage()
	private var incompleteSpeed = PositiveAverage()
	private var incompletePower = PositiveAverage()
	private var incompleteDragFactor = PositiveAverage()
	
	private var splitTimes = [Float]()
	private var splitDistances = [Float]()
	
	func feed(_ buffer: RowingBuffer) {
		// determine split length
		if buffer.intervalCount == 0 {
			splitLength = buffer.distance.roundToNearest10
		}
		
		intervalRestTime = max(intervalRestTime, buffer.restTime)
		intervalRestDistance = max(intervalRestDistance, buffer.restDistance)
		splitIntervalTime = buffer.lastSplitTime
		splitIntervalPower = buffer.splitAvgPower > 0 ? buffer.splitAvgPower : splitIntervalPower
		splitIntervalAverageStrokeRate = buffer.splitIntervalAverageStrokeRate > 0 ? buffer.splitIntervalAverageStrokeRate : splitIntervalAverageStrokeRate
		
		splitIntervalDistance = splitLength
		splitIntervalAveragePace = 0
		
		if splitLength > 0 {
			splitIntervalAveragePace = splitIntervalTime.magicPM5RoundingUp * 500 / splitLength
		}
		
		if buffer.intervalType.isWorkInterval {
			splitIntervalType = buffer.intervalType
		}

		splitIntervalTotalCalories = buffer.splitIntervalTotalCalories
		splitIntervalAverageCalories = buffer.splitIntervalAverageCalories
		splitIntervalSpeed = buffer.splitIntervalSpeed
		splitIntervalAverageDragFactor = buffer.splitIntervalAverageDragFactor
		
		// incomplete data
		elapsedTime = buffer.elapsedTime
		elapsedDistance = buffer.distance
		incompleteStrokeRate.add(value: Float(buffer.strokeRate))
		incompleteCalories.add(value: buffer.strokeCalories)
		incompleteSpeed.add(value: buffer.speed)
		incompletePower.add(value: buffer.strokePower)
		incompleteDragFactor.add(value: buffer.dragFactor)
	}
	
	func resolve(_ buffer: IntervalBuffer) {
		buffer.elapsedTime = splitIntervalTime
		buffer.elapsedDistance = splitIntervalDistance
		
		// incomplete data
		splitTimes.append(splitIntervalTime)
		splitDistances.append(splitIntervalDistance)
		
		buffer.restTime = intervalRestTime
		buffer.restDistance = intervalRestDistance
		buffer.intervalType = splitIntervalType
		buffer.avgStrokeRate = splitIntervalAverageStrokeRate
		buffer.avgPace = splitIntervalAveragePace
		buffer.totalCalories = splitIntervalTotalCalories
		buffer.avgCalories = splitIntervalAverageCalories
		buffer.avgSpeed = splitIntervalSpeed
		buffer.avgPower = splitIntervalPower
		buffer.avgDragFactor = splitIntervalAverageDragFactor
		
		reset()
	}
	
	func resolveIncomplete(_ buffer: IntervalBuffer) {
		let pastTime = splitTimes.reduce(0, +)
		let time = elapsedTime - pastTime
		buffer.elapsedTime = time
		
		let pastDistance = splitDistances.reduce(0, +)
		let distance = floorf(elapsedDistance - pastDistance)
		buffer.elapsedDistance = distance
		
		buffer.restTime = intervalRestTime
		buffer.restDistance = intervalRestDistance
		buffer.intervalType = splitIntervalType
		
		buffer.avgStrokeRate = incompleteStrokeRate.getAverage()
		buffer.avgPace = time.magicPM5RoundingUp * 500 / distance
		buffer.totalCalories = incompleteCalories.getTotal()
		buffer.avgCalories = incompleteCalories.getAverage()
		buffer.avgSpeed = incompleteSpeed.getAverage()
		buffer.avgPower = incompletePower.getAverage()
		buffer.avgDragFactor = incompleteDragFactor.getAverage()

		reset()
	}
	
	private func reset() {
		splitIntervalTime = 0
		intervalRestTime = 0
		intervalRestDistance = 0
		splitIntervalType = .none
		splitIntervalNumber = 0
		splitIntervalAverageStrokeRate = 0
		splitIntervalAveragePace = 0
		splitIntervalTotalCalories = 0
		splitIntervalAverageCalories = 0
		splitIntervalSpeed = 0
		splitIntervalPower = 0
		splitIntervalAverageDragFactor = 0
		
		// incomplete data
		elapsedTime = 0
		elapsedDistance = 0
		incompleteStrokeRate = PositiveAverage()
		incompleteCalories = PositiveAverage()
		incompleteSpeed = PositiveAverage()
		incompletePower = PositiveAverage()
		incompleteDragFactor = PositiveAverage()
	}
}

class SingleDistanceSessionResolver: SessionResolver {
	private var totalWorkTime: Float = 0.0
	private var totalWorkDistance: Float = 0.0
	
	func feed(_ buffer: RowingBuffer) {
		totalWorkDistance = max(buffer.distance, totalWorkDistance)
		totalWorkTime = buffer.elapsedTime
	}
	
	func resolve(_ buffer: SessionBuffer) {
		buffer.distance = totalWorkDistance
		buffer.elapsedTime = totalWorkTime.rounded(toPlaces: 1)
		
		reset()
	}
	
	private func reset() {
		totalWorkDistance = 0
		totalWorkTime = 0
	}
}

class JustRowResolver: IntervalResolver {
	private var splitIntervalTime: Float = 0.0
	private var splitIntervalDistance: Float = 0.0
	private var splitIntervalAverageStrokeRate: Float = 0.0
	private var splitIntervalAveragePace: Float = 0.0
	private var splitIntervalTotalCalories: Float = 0.0
	private var splitIntervalAverageCalories: Float = 0.0
	private var splitIntervalSpeed: Float = 0.0
	private var splitIntervalPower: Float = 0.0
	private var splitIntervalAverageDragFactor: Float = 0.0
	
	func feed(_ buffer: RowingBuffer) {
		splitIntervalTime = buffer.elapsedTime > 0 ? buffer.elapsedTime : splitIntervalTime
		splitIntervalPower = buffer.splitAvgPower > 0 ? buffer.splitAvgPower : splitIntervalPower
		splitIntervalAverageStrokeRate = buffer.splitIntervalAverageStrokeRate > 0 ? buffer.splitIntervalAverageStrokeRate : splitIntervalAverageStrokeRate
		
		splitIntervalDistance = buffer.distance > 0 ? buffer.distance : splitIntervalDistance
		splitIntervalAveragePace = 0
		if splitIntervalDistance > 0 {
			splitIntervalAveragePace = splitIntervalTime.magicPM5RoundingUp * 500 / splitIntervalDistance
		}
		
		splitIntervalTotalCalories = buffer.splitIntervalTotalCalories
		splitIntervalAverageCalories = buffer.splitIntervalAverageCalories
		splitIntervalSpeed = buffer.splitIntervalSpeed
		splitIntervalAverageDragFactor = buffer.splitIntervalAverageDragFactor
	}
	
	func resolve(_ buffer: IntervalBuffer) {
		buffer.elapsedTime = splitIntervalTime
		buffer.elapsedDistance = splitIntervalDistance
		buffer.intervalType = IntervalType(rawValue: 1)
		buffer.avgStrokeRate = splitIntervalAverageStrokeRate
		buffer.avgPace = splitIntervalAveragePace
		buffer.totalCalories = splitIntervalTotalCalories
		buffer.avgCalories = splitIntervalAverageCalories
		buffer.avgSpeed = splitIntervalSpeed
		buffer.avgPower = splitIntervalPower
		buffer.avgDragFactor = splitIntervalAverageDragFactor
		reset()
	}
	
	// not used
	func resolveIncomplete(_ buffer: IntervalBuffer) {}

	private func reset() {
		splitIntervalTime = 0
		splitIntervalAverageStrokeRate = 0
		splitIntervalAveragePace = 0
		splitIntervalTotalCalories = 0
		splitIntervalAverageCalories = 0
		splitIntervalSpeed = 0
		splitIntervalPower = 0
		splitIntervalAverageDragFactor = 0
	}
}

class JustRowSessionResolver: SessionResolver {
	private var totalTime: Float = 0.0
	private var totalDistance: Float = 0.0
	
	func feed(_ buffer: RowingBuffer) {
		totalTime = buffer.elapsedTime > 0 ? buffer.elapsedTime : totalTime
		totalDistance = buffer.distance > 0 ? buffer.distance : totalDistance
	}
	
	func resolve(_ buffer: SessionBuffer) {
		buffer.distance = totalDistance
		buffer.elapsedTime = totalTime
	}
}
