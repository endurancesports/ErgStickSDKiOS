//
//  SessionRecorder.swift
//  Float
//
//  Created by Nikola Milicevic on 4/29/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

class SessionRecorder {
	
	// MARK: - Properties
	
	private var session: SessionBuffer?
	private var isRecording: Bool {
		return session != nil
	}
	private var strokeCount = 0
	private var bufferStrokeCount = 0
	
	// used only for float server upload
	private var sampleCount = 0
	
	// used for seconds passed
	private var pastTime: Float = 0
	
	// MARK: - Recording
	
	func record(_ buffer: RowingBuffer, isErgstick: Bool = false) {
		if !isRecording { createSession() }
		
		// if PM sent non valid data - exit
		guard buffer.strokeCount != 0 else { return }
		
		// calculate stroke count
		if bufferStrokeCount != buffer.strokeCount {
			bufferStrokeCount = buffer.strokeCount
			strokeCount += 1
		}
		
		// calculate sample count
		sampleCount += 1
		
		// calculate stroke based on stroke count
		if session?.strokes?.last?.internalId == strokeCount {
			// update certain properties every time
			session!.strokes![session!.strokes!.count - 1].driveLength = buffer.driveLength
			session!.strokes![session!.strokes!.count - 1].driveTime = buffer.driveTime
			session!.strokes![session!.strokes!.count - 1].recoveryTime = buffer.strokeRecoveryTime
			session!.strokes![session!.strokes!.count - 1].peakDriveForce = buffer.peakDriveForce
			session!.strokes![session!.strokes!.count - 1].avgDriveForce = buffer.avgDriveForce
			session!.strokes![session!.strokes!.count - 1].timeToPeak = calculateTimeToPeak(buffer)
			session!.strokes![session!.strokes!.count - 1].plot = buffer.plot
		} else {
			// create new stroke if it doesn't exist
			let stroke = StrokeBuffer(internalId: strokeCount, driveLength: buffer.driveLength, driveTime: buffer.driveTime, recoveryTime: buffer.strokeRecoveryTime, distance: buffer.strokeDistance, peakDriveForce: buffer.peakDriveForce, avgDriveForce: buffer.avgDriveForce, workPerStroke: buffer.workPerStroke, power: buffer.strokePower, timeToPeak: calculateTimeToPeak(buffer), calories: buffer.strokeCalories, projectedWorkTime: buffer.projectedWorkTime, projectedWorkDistance: buffer.projectedWorkDistance, plot: buffer.plot)
			
			session?.strokes?.append(stroke)
		}
		
		// calculate secconds passed
		var secondsPassed: Float = 0.0
		if buffer.workoutType.isIntervalBased {
			secondsPassed = buffer.elapsedTime + pastTime
		} else {
			secondsPassed = buffer.elapsedTime
		}
		
		// append sample
        let sample = SampleBuffer(internalId: sampleCount, strokeInternalId: (session?.strokes?.count)!, secondsPassed: secondsPassed, elapsedTime: buffer.elapsedTime, distance: buffer.distance, workoutState: buffer.workoutState, rowingState: buffer.rowingState, strokeState: buffer.strokeState, totalWorkDistance: buffer.totalWorkDistance, workoutDuration: buffer.workoutDuration, workoutDistance: buffer.workoutDistance, dragFactor: buffer.dragFactor, speed: buffer.speed, strokeRate: buffer.strokeRate, currentHeartRate: buffer.currentHeartRate, currentPace: buffer.currentPace, avgPace: buffer.avgPace, restDistance: buffer.restDistance, restTime: buffer.restTime, totalAvgPower: buffer.totalAvgPower, totalAvgCalories: buffer.totalAvgCalories, splitAvgPace: buffer.splitAvgPace, splitAvgPower: buffer.splitAvgPower, splitAvgCalories: buffer.splitAvgCalories, lastSplitTime: buffer.lastSplitTime, lastSplitDistance: buffer.lastSplitDistance)
		
		session?.intervals?.last?.samples?.append(sample)
		
		// ergstick fix
		if isErgstick {
			// rest time
			session?.intervals?.last?.restTime = sample.restTime
			
			// rest distance
			guard let intervals = session?.intervals, intervals.count >= 2 else { return }
			if sample.restDistance != 0 {
				intervals[intervals.count - 2].restDistance = sample.restDistance
			}
		}
	}
	
	func record(_ buffer: IntervalBuffer) {
        //print("record(_ buffer: IntervalBuffer) type: \(buffer.intervalType!.displayString), restTime: \(buffer.restTime), buffer.restDistance: \(buffer.restDistance)")
		session?.intervals?.last?.internalId = session?.intervals?.count
        session?.intervals?.last?.intervalType = buffer.intervalType
		session?.intervals?.last?.elapsedTime = buffer.elapsedTime
		session?.intervals?.last?.elapsedDistance = buffer.elapsedDistance
		session?.intervals?.last?.restTime = buffer.restTime
		session?.intervals?.last?.restDistance = buffer.restDistance
		session?.intervals?.last?.avgStrokeRate = buffer.avgStrokeRate
		session?.intervals?.last?.avgHeartRate = buffer.avgHeartRate
		session?.intervals?.last?.avgPace = buffer.avgPace
		session?.intervals?.last?.totalCalories = buffer.totalCalories
		session?.intervals?.last?.avgCalories = buffer.avgCalories
		session?.intervals?.last?.avgSpeed = buffer.avgSpeed
		session?.intervals?.last?.avgPower = buffer.avgPower
		session?.intervals?.last?.avgDragFactor = buffer.avgDragFactor
		session?.intervals?.last?.plannedDistanceOrTime = buffer.plannedDistanceOrTime
		
		calculatePastTime()
		
		makeNewInterval()
	}
	
	func record(_ buffer: SessionBuffer) {
        print("record(_ buffer: SessionBuffer)")
		removeLastEmptyInterval()
		
		session?.guid = makeGUID()
		session?.deviceIdentifier = buffer.deviceIdentifier
		session?.deviceFirmware = buffer.deviceFirmware
		session?.programId = 0
		session?.workoutType = buffer.workoutType
		session?.createdAt = makeDate()
		
		session?.elapsedTime = buffer.elapsedTime
		session?.distance = buffer.distance
		session?.avgPace = buffer.avgPace
		session?.avgPower = buffer.avgPower
		session?.avgStrokeRate = buffer.avgStrokeRate
		session?.endingHeartRate = buffer.endingHeartRate
		session?.avgHeartRate = buffer.avgHeartRate
		session?.minHeartRate = buffer.minHeartRate
		session?.maxHeartRate = buffer.maxHeartRate
		session?.dragFactorAvg = buffer.dragFactorAvg
		
        // avgPace can be infinity so set it to 0.0
        session?.intervals?.forEach({ (interval) in
            if !interval.avgPace.isNormal { interval.avgPace = 0.0 }
        })
		guard let data = encodeSession() else { return }
		NotificationCenter.default.post(name: .didRecordSession, object: data)
	}
	
	func recordErgstick(_ buffer: SessionBuffer) {
        //print("recordErgstick(_ buffer: SessionBuffer), session?.intervals.count: \(session?.intervals?.count)")
//        (session?.intervals ?? []).forEach { (intervalbuff) in
//            print("kraj: \(intervalbuff.intervalType?.displayString), restDistance: \(intervalbuff.restDistance), restTime: \(intervalbuff.restTime)")
//        }
		removeLastEmptyInterval()
		removeLastIntervalIfTooShort()
//        (session?.intervals ?? []).forEach { (intervalbuff) in
//            print("kraj posle: \(intervalbuff.intervalType?.displayString), restDistance: \(intervalbuff.restDistance), restTime: \(intervalbuff.restTime)")
//        }
		guard let intervals = session?.intervals, let strokes = session?.strokes, let workoutType = buffer.workoutType else { return }
		
		// time
		var eowElapsedTime = buffer.elapsedTime
		if workoutType.isIntervalBased {
			var time: Float = 0
			for interval in intervals {
				time += interval.elapsedTime.magicPM5RoundingUp
			}
			eowElapsedTime = time
		}
		
		// distance, rate
		var eowDistance: Float = 0
		var eowRate: Float = 0
		for interval in intervals {
			if workoutType == .fixedDistanceIntervals {
				eowDistance += interval.elapsedDistance
			}
			eowRate += interval.avgStrokeRate
		}
		if workoutType != .fixedDistanceIntervals {
			eowDistance = buffer.distance
		}
		if workoutType == .variableIntervals || workoutType == .variableIntervalsRestUndefined {
			eowDistance = 0
			for interval in intervals {
				eowDistance += interval.elapsedDistance
			}
		}
		eowDistance = floorf(eowDistance)
		eowRate = eowRate / Float(intervals.count)
		
		// dragFactor
		let eowDragFactorAverage = WorkoutRunningAverage()
		for temp in intervals {
			if let intervalSamples = temp.samples {
				for sample in intervalSamples {
					eowDragFactorAverage.add(value: sample.dragFactor, workoutState: sample.workoutState.rawValue)
				}
			}
		}
		
		// power
		let eowAvgPower = WorkoutRunningAverage()
		for temp in strokes {
			eowAvgPower.add(value: temp.power, workoutState: 1)
		}
		
		session?.guid = makeGUID()
		session?.deviceIdentifier = buffer.deviceIdentifier
		session?.deviceFirmware = buffer.deviceFirmware
		session?.programId = 0
		session?.workoutType = buffer.workoutType
		session?.createdAt = makeDate()
		
		session?.elapsedTime = eowElapsedTime
		session?.distance = eowDistance
		session?.avgPace = (eowElapsedTime.magicPM5RoundingUp * 500.0) / eowDistance
		session?.avgPower = eowAvgPower.getAverage()
		session?.avgStrokeRate = eowRate
		session?.endingHeartRate = buffer.endingHeartRate
		session?.avgHeartRate = buffer.avgHeartRate
		session?.minHeartRate = buffer.minHeartRate
		session?.maxHeartRate = buffer.maxHeartRate
		session?.dragFactorAvg = eowDragFactorAverage.getAverage()
		
        // avgPace can be infinity so set it to 0.0
        session?.intervals?.forEach({ (interval) in
            if !interval.avgPace.isNormal { interval.avgPace = 0.0 }
        })
		guard let data = encodeSession() else { return }
		NotificationCenter.default.post(name: .didRecordSession, object: data)
	}
}

private extension SessionRecorder {
	func createSession() {
		makeNewSession()
		makeNewInterval()
	}
	
	func makeNewSession() {
		session = SessionBuffer()
		session?.intervals = [IntervalBuffer]()
		session?.strokes = [StrokeBuffer]()
	}
	
	func makeNewInterval() {
		print(#function)
		session?.intervals?.append(IntervalBuffer())
		session?.intervals?.last?.samples = [SampleBuffer]()
	}
	
    func removeLastEmptyInterval() {
        guard let sessionIntervals = session?.intervals, !sessionIntervals.isEmpty else { return }
        
        if session?.intervals?.last?.intervalType == nil || session?.intervals?.last?.intervalType == IntervalType.none {
            session?.intervals?.removeLast()
        } else {
            return
        }
        
        removeLastEmptyInterval()
    }
	
	func removeLastIntervalIfTooShort() {
        guard var intervals = session?.intervals, !intervals.isEmpty else { return }
        
		guard let distance = intervals.last?.elapsedDistance else {
			intervals.removeLast()
			return
		}
		
		if distance < 1 {
			intervals.removeLast()
		}
	}
	
	func makeGUID() -> String {
		return "ios-\(Int(Date().timeIntervalSince1970))-\(UUID().uuidString.lowercased())"
	}
	
	func makeDate() -> String {
		return Date().toString(format: .createdAt)
	}
	
	func calculateTimeToPeak(_ buffer: RowingBuffer) -> Float {
		guard !buffer.plot.isEmpty else { return 0 }
		
		var max: Float = 0
		var index = 0
		for (i, x) in buffer.plot.enumerated() {
			if x > max {
				max = x
				index = i
			}
		}
		
		return (Float(index + 1) / Float(buffer.plot.count)) * 100
	}
	
	func calculatePastTime() {
		guard let interval = session?.intervals?.last else { return }
		pastTime = pastTime + interval.elapsedTime + interval.restTime
	}
	
	func encodeSession() -> Data? {
		do {
			let encoder = JSONEncoder()
			encoder.keyEncodingStrategy = .convertToSnakeCase
			let data = try encoder.encode(session)
			return data
		} catch let error {
			print("encoding error:", error)
			return nil
		}
	}
}
