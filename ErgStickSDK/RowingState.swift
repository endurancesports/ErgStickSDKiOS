//
//  RowingState.swift
//  Float
//
//  Created by Nikola Milicevic on 6/20/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

@objc public enum RowingState: Int16, Codable {
	case inactive
	case active
	
	public var displayString: String {
		switch self {
		case .inactive: return "Inactive"
		case .active: return "Active"
		}
	}
}
