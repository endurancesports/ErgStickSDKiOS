//
//  ErgStickSDK.h
//  ErgStickSDK
//
//  Created by Nikola Milicevic on 7/27/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for ErgStickSDK.
FOUNDATION_EXPORT double ErgStickSDKVersionNumber;

//! Project version string for ErgStickSDK.
FOUNDATION_EXPORT const unsigned char ErgStickSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <ErgStickSDK/PublicHeader.h>


