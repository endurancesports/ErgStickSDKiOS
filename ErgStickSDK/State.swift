//
//  State.swift
//  ErgStick
//
//  Created by Nikola Milicevic on 12/17/17.
//  Copyright © 2017 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation

class State {
	private var currentState: WorkoutState.WorkoutStateType = .waitToBegin
	private var previousState: WorkoutState.WorkoutStateType = .waitToBegin
	
	var isWaitToBegin: Bool {
		return currentState == .waitToBegin
	}
	var hasChangedToWaitToBegin: Bool {
		return currentState == .waitToBegin && previousState != .waitToBegin
	}
	var hasFinished: Bool {
		return currentState == .ended || currentState == .logged
	}
	var isRunning: Bool {
		return currentState == .running
	}
	var hasChangedFromRunning: Bool {
		return previousState == .running && currentState != .running
	}
	var hasTerminatedJustRow: Bool {
		return (previousState == .terminated && currentState == .waitToBegin) || (previousState == .running && currentState == .waitToBegin)
	}
	
	func setState(_ value: WorkoutState.WorkoutStateType) {
		previousState = currentState
		currentState = value
	}
	
	func getCurrentState() -> WorkoutState.WorkoutStateType {
		return currentState
	}
	
	func getPreviousState() -> WorkoutState.WorkoutStateType {
		return previousState
	}

	// MARK: - Resetting UI
	
	private var currentWorkoutType: WorkoutType = .justRowSplits
	private var previousWorkoutType: WorkoutType = .justRowSplits
	
	private var currentIntervalType: IntervalType = .distance
	private var previousIntervalType: IntervalType = .distance
	
	private var currentWorkoutDistanceOrTime: Float = 0.0
	private var previousWorkoutDistanceOrTime: Float = 0.0
	
	var isDistanceOrTimeChanged: Bool {
		return (previousWorkoutDistanceOrTime != currentWorkoutDistanceOrTime)
	}
	var isWorkoutJustRow: Bool {
		return previousWorkoutDistanceOrTime == 0 && currentWorkoutDistanceOrTime == 0
	}
	var isWorkoutTypeChanged: Bool {
		return currentWorkoutType != previousWorkoutType
	}
	var isIntervalTypeChanged: Bool {
		return currentIntervalType != previousIntervalType
	}
	
	func setWorkoutType(_ value: WorkoutType) {
		previousWorkoutType = currentWorkoutType
		currentWorkoutType = value
	}
	
	func getCurrentWorkoutType() -> WorkoutType {
		return currentWorkoutType
	}
	
	func setIntervalType(_ value: IntervalType) {
		previousIntervalType = currentIntervalType
		currentIntervalType = value
	}
	
	func setWorkoutDistanceOrTime(_ value: Float) {
		previousWorkoutDistanceOrTime = currentWorkoutDistanceOrTime
		currentWorkoutDistanceOrTime = value
	}
}
