//
//  Extensions.swift
//  ErgStick
//
//  Created by Nikola Milicevic on 10/4/17.
//  Copyright © 2017 Endurance Sports Research Ltd. All rights reserved.
//

import UIKit
import CoreBluetooth

extension UInt8 {
	var char: Character {
		return Character(UnicodeScalar(self))
	}
}

public extension Float {
	var minutesSeconds: String {
		let seconds = floor(self)
		if seconds < 60.0 {
			return String.init(format: "0:%02.0f", floor(self))
		}
		return String.init(format: "%.0f:%02.0f", floor(seconds / 60.0), seconds - 60.0 * floor(seconds/60.0))
	}
	var minutesSecondsDetail: String {
		let seconds = floor(self * 10.0 + 0.5) / 10.0
		if seconds < 60.0 {
			return String.init(format: "0:%04.1f", seconds)
		}
		return String.init(format: "%.0f:%04.1f", floor(seconds / 60.0), seconds - 60.0 * floor(seconds / 60.0))
	}
	var hoursMinutesSeconds: String {
		let seconds = floor(self)
		if seconds < 60.0 {
			return String.init(format: "0:%02.0f", seconds)
		}
		if seconds < 3600.0 {
			return minutesSeconds
		}
		
		let hours = floor(seconds/3600.0)
		let minutes = floor((seconds - 3600.0 * hours) / 60.0)
		let sec = seconds - 3600.0 * hours - 60.0 * minutes
		
		return String.init(format: "%.0f:%02.0f:%02.0f", hours, minutes, sec)
	}
	var hoursMinutesSecondsDetail: String {
		let seconds = floor(self * 10.0 + 0.5) / 10.0
		
		if seconds < 60.0 {
			return String.init(format: "0:%04.1f", seconds)
		}
		if seconds < 3600.0 {
			return minutesSecondsDetail
		}
		
		let hours = floor(seconds / 3600.0)
		let minutes = floor((seconds - 3600.0 * hours) / 60.0)
		let sec = seconds - 3600.0 * hours - 60.0 * minutes
		
		return String.init(format: "%.0f:%02.0f:%04.1f", hours, minutes, sec)
	}
	var properUnits: Float {
		if PeripheralManager.shared.weightUnits == .metric {
			return self * Constants.Units.lbs
		} else {
			return self
		}
	}
	var distanceString: String {
		return String.init(format: "%.0f m", floor(self))
	}
	func string(decimals: Int) -> String {
		return String.init(format: "%.\(decimals)f", self)
	}
}

extension Float {
	func rounded(toPlaces places:Int) -> Float {
		let divisor = pow(10.0, Float(places))
		return (self * divisor).rounded() / divisor
	}
	var magicPM5RoundingUp: Float {
		return floor((self * 100).rounded() / 10) / 10
	}
	var roundToNearest10: Float {
		return floor(self / 10 + 0.5) * 10.0
	}
	init(with2BytesOf data: Data, startingFrom index: Int, multiplyBy factor: Float) {
		let a = UInt32(data[index])
		let b = UInt32(data[index + 1])
		let value = a | b<<8
		self = Float(value) * factor
	}
	init(with2BytesBigEndianOf data: Data, startingFrom index: Int, multiplyBy factor: Float) {
		let a = UInt32(data[index + 1])
		let b = UInt32(data[index])
		let value = a | b<<8
		self = Float(value) * factor
	}
	init(with3BytesOf data: Data, startingFrom index: Int, multiplyBy factor: Float) {
		let a = UInt32(data[index])
		let b = UInt32(data[index + 1])
		let c = UInt32(data[index + 2])
		let value = a | b<<8 | c<<16
		self = Float(value) * factor
	}
	init(with4BytesBigEndianOf data: Data, startingFrom index: Int, multiplyBy factor: Float) {
		let a = UInt32(data[index + 3])
		let b = UInt32(data[index + 2])
		let c = UInt32(data[index + 1])
		let d = UInt32(data[index])
		let value = a | b<<8 | c<<16 | d<<24
		self = Float(value) * factor
	}
	init(with5BytesOf data: Data, startingFrom index: Int, multiplyBy factor: Float) {
		let a = UInt32(data[index])
		let b = UInt32(data[index + 1])
		let c = UInt32(data[index + 2])
		let d = UInt32(data[index + 3])
		let value = a | b<<8 | c<<16 | d<<24
		self = (Float(value) + Float(data[index + 4])) * factor
	}
}

public extension Notification.Name {
	static var peripheralConnectivityDidChange: Notification.Name {
		return Notification.Name(rawValue: "peripheralConnectivityDidChange")
	}
	static var didDiscoverPeripheral: Notification.Name {
		return Notification.Name(rawValue: "didDiscoverPeripheral")
	}
	static var newErgStickFirmwareAvailable: Notification.Name {
		return Notification.Name(rawValue: "newErgStickFirmwareAvailable")
	}
	static var newPMFirmwareAvailable: Notification.Name {
		return Notification.Name(rawValue: "newPMFirmwareAvailable")
	}
	static var newPM5FirmwareAvailable: Notification.Name {
		return Notification.Name(rawValue: "newPM5FirmwareAvailable")
	}
	static var newRowingDataAvailable: Notification.Name {
		return Notification.Name(rawValue: "newRowingDataAvailable")
	}
	static var newForceCurveDataAvailable: Notification.Name {
		return Notification.Name(rawValue: "newForceCurveDataAvailable")
	}
	static var resetRowingData: Notification.Name {
		return Notification.Name(rawValue: "resetRowingData")
	}
	static var didRecordSession: Notification.Name {
		return Notification.Name(rawValue: "didRecordSession")
	}
}

extension CBPeripheral {
	var isErgStick: Bool {
		if let _ = name, name!.uppercased().contains("ERGSTICK") {
			return true
		}
		return false
	}
	var isPM5: Bool {
		if let _ = name, name!.uppercased().contains("PM5") {
			return true
		}
		return false
	}
}

extension CBCharacteristic {
	var isHardwareVersion: Bool {
		return uuid.uuidString == Constants.PM5.InformationService.hardwareVersion
	}
	var isFirmwareVersion: Bool {
		return uuid.uuidString == Constants.ErgStick.InformationService.firmwareVersion
	}
	var isWrite: Bool {
		return uuid.uuidString == Constants.PM5.ControlPrimaryService.write
	}
	var isRead: Bool {
		return uuid.uuidString == Constants.PM5.ControlPrimaryService.read
	}
	var isGeneralStatus: Bool {
		return uuid.uuidString == Constants.PM5.RowingService.generalStatus
	}
	var isAdditionalStatus1: Bool {
		return uuid.uuidString == Constants.PM5.RowingService.additionalStatus1
	}
	var isAdditionalStatus2: Bool {
		return uuid.uuidString == Constants.PM5.RowingService.additionalStatus2
	}
	var isStrokeData: Bool {
		return uuid.uuidString == Constants.PM5.RowingService.strokeData
	}
	var isAdditionalStrokeData: Bool {
		return uuid.uuidString == Constants.PM5.RowingService.additionalStrokeData
	}
	var isSplitInterval: Bool {
		return uuid.uuidString == Constants.PM5.RowingService.splitIntervalData
	}
	var isAdditionalSplitInterval: Bool {
		return uuid.uuidString == Constants.PM5.RowingService.additionalSplitIntervalData
	}
	var isEndOfWorkout: Bool {
		return uuid.uuidString == Constants.PM5.RowingService.endOfWorkoutSummaryData
	}
	var isEndOfWorkoutAdditional: Bool {
		return uuid.uuidString == Constants.PM5.RowingService.endOfWorkoutAdditionalSummaryData
	}
	var isForceCurve: Bool {
		return uuid.uuidString == Constants.PM5.RowingService.forceCurve
	}
	var isHeartRate: Bool {
		return uuid.uuidString == Constants.ErgStick.InformationService.heartRate
	}
	var isHeartRateMonitorId: Bool {
		return uuid.uuidString == Constants.ErgStick.InformationService.heartRateMonitorId
	}
}

extension Data {
	var hexDescription: String {
		return reduce("") {$0 + String(format: "%02x ", $1)}
	}
	
	func checkSum() -> Int {
		return self.map { Int($0) }.reduce(0, +) & 0xff
	}
}

public extension Date {
	enum DateFormat: String {
		case createdAt = "yyyy-MM-dd HH:mm:ss"
		case display = "E d/MMM/y"
	}
	
	static func from(string: String, format: DateFormat) -> Date? {
		let formatter = DateFormatter()
        formatter.calendar   = Calendar(identifier: .iso8601)
        formatter.timeZone   = TimeZone.current
        formatter.locale     = Locale(identifier: "en_US_POSIX")
		formatter.dateFormat = format.rawValue
		return formatter.date(from: string)
	}
	
	func toString(format: DateFormat) -> String {
        let formatter = DateFormatter()
        formatter.calendar   = Calendar(identifier: .iso8601)
        formatter.timeZone   = TimeZone.current
        formatter.locale     = Locale(identifier: "en_US_POSIX")
		formatter.dateFormat = format.rawValue
		return formatter.string(from: self)
	}
}
