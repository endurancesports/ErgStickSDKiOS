//
//  HeartRateMonitor.swift
//  ErgStick
//
//  Created by Nikola Milicevic on 3/4/18.
//  Copyright © 2018 Endurance Sports Research Ltd. All rights reserved.
//

import Foundation
import CoreBluetooth

class HeartRateMonitor: HeartRatePeripheral {
	
	// MARK: - Properties
	
	var peripheral: CBPeripheral
	let services = [
		CBUUID(string: Constants.HeartRateMonitor.HeartRateService.base)
	]
	let characteristics = [
		CBUUID(string: Constants.HeartRateMonitor.HeartRateService.measurement),
	]
	
	var bpm: Float = 0.0
	
	// MARK: - Init
	
	required init(peripheral: CBPeripheral) {
		self.peripheral = peripheral
	}

	// MARK: - Data handling
	
	func peripheral(_ peripheral: CBPeripheral, didDiscover characteristic: CBCharacteristic) {
		if characteristic.uuid.uuidString == Constants.HeartRateMonitor.HeartRateService.measurement {
			peripheral.setNotifyValue(true, for: characteristic)
		}
	}
	
	func peripheral(_ peripheral: CBPeripheral, didUpdateValueFor characteristic: CBCharacteristic) {
		parse(characteristic)
	}
}

private extension HeartRateMonitor {
	func parse(_ characteristic: CBCharacteristic) {
		guard let data = characteristic.value, data.count > 1 else { return }
		
		if data[0] & 0x01 == 0 {
			bpm = Float(data[1])
		} else {
			bpm = Float(CFSwapInt16BigToHost(UInt16(data[1])))
		}
	}
}
